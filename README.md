# sunnomads-api
La api de la app para sunnomads

##Install
----
* Clone repository
`git clone (your_bitbucket_url)`
* move to your new directory
`cd ./your_new_repository`
* install dependencies
`composer install`
* Update your etc/hosts
`sudo nano /etc/hosts`
```
192.168.10.10 sunnomads.local
```
* Update your homestead.yaml
`homestead edit`
```
- map: helpers.local
  to: /home/vagrant/workspace/sunnomads-api-rest/public
```
* Update your homestead.yaml
`homestead edit`
```
- sunnomads
```
* copy the .env
`cp .env.example .env`
* edit the .env
`nano .env`
* launch homestead
if you want to reload `homestead reload --provision`
if you want to up `homestead up --provision`
