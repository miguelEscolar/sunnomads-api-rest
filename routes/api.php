<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// OAuth Routes
Route::post('login', 'AuthController@loginEmail');
Route::post('register', 'AuthController@registerEmail');

Route::get('recaudacion', 'UserController@getMoney');
Route::get('countries', 'RetoController@getPaises');


//JWT Token need for this group
Route::group(['middleware' => 'jwt.auth'], function () {

    Route::get('position/{pais}', 'RetoController@saveOutPosition');
    Route::get('pilots', 'UserController@getPilots');
    Route::post('recaudacion', 'UserController@saveMoney');

    //Auth routes
    Route::post('changePassword', 'AuthController@changePassword');
    Route::post('changeEmail', 'AuthController@changeEmail');

    //User routes
    Route::post('user/edit', 'UserController@editUser');
    Route::get('user', 'UserController@getUser');
    Route::post('user/vote/{reto}', 'UserController@vote');
    Route::post('chat/write', 'UserController@addComment');
    Route::get('ranking', 'UserController@getRanking');
    Route::get('chat/get', 'UserController@getComments');

    //Retos routes
    Route::post('reto/create', 'RetoController@createReto');
    Route::post('reto/edit/{reto}', 'RetoController@editReto');
    Route::post('reto/estado/{reto}', 'RetoController@cambiarEstadoReto');
    Route::get('retos/public', 'RetoController@getRetos');
    Route::get('retos', 'RetoController@getAllRetos');
    Route::get('reto/get/{reto}', 'RetoController@getReto');

    //Pin routes
    Route::post('Pin/create', 'PinController@createPin');
    Route::post('Pin/addFiles/{pin}', 'PinController@addFiles');
    Route::post('Pin/edit/{pin}', 'PinController@editPin');
    Route::get('pines', 'PinController@getPines');
    Route::get('pin/get/{pin}', 'PinController@getPin');
    Route::delete('pin/delete/{pin}', 'PinController@deletePin');
});
