<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Starting to create tables relations".PHP_EOL;

        echo "Creating users foreign keys".PHP_EOL;
        Schema::table('users', function (Blueprint $table) {
            $table->integer('rol_id')->unsigned()->nullable()->after('id');

            $table->foreign('rol_id')->references('id')->on('roles')
                ->onDelete('set null');
        });

        echo "Creating votos foreign keys".PHP_EOL;
        Schema::table('votos', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->after('id');
            $table->integer('estado_id')->unsigned()->after('id');
            $table->integer('reto_id')->unsigned()->after('id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('estado_id')->references('id')->on('estados');
            $table->foreign('reto_id')->references('id')->on('retos');
        });

        echo "Creating pines foreign keys".PHP_EOL;
        Schema::table('pines', function (Blueprint $table) {
          $table->integer('tipo_id')->unsigned()->after('id');
          $table->integer('user_id')->unsigned()->after('id');

          $table->foreign('tipo_id')->references('id')->on('tipos');
          $table->foreign('user_id')->references('id')->on('users');
        });

        echo "Creating retos foreign keys".PHP_EOL;
        Schema::table('retos', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable()->after('id');
            $table->integer('estado_id')->unsigned()->after('id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('estado_id')->references('id')->on('estados');
        });

        echo "Creating ranking foreign keys".PHP_EOL;
        Schema::table('rankings', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->after('id');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "Starting to drop all foreign keys".PHP_EOL;
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['rol_id']);
            $table->dropColumn('rol_id');
        });

        Schema::table('votos', function (Blueprint $table) {
            $table->dropForeign(['estado_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['reto_id']);
            $table->dropColumn('user_id');
            $table->dropColumn('reto_id');
            $table->dropColumn('estado_id');
        });

        Schema::table('pines', function (Blueprint $table) {
            $table->dropForeign(['tipo_id']);
            $table->dropColumn('tipo_id');
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });

        Schema::table('retos', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropForeign(['estado_id']);
            $table->dropColumn('estado_id');
        });

        Schema::table('rankings', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });
    }
}
