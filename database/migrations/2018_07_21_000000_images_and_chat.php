<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImagesAndChat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      echo "Creating chat table".PHP_EOL;

      Schema::create('chats', function (Blueprint $table) {
          $table->increments('id');
          $table->string('msg');
          $table->timestamps();
      });

      echo "Creating pin_files table".PHP_EOL;
      Schema::create('pin_files', function (Blueprint $table) {
          $table->increments('id');
          $table->string('archivo');
          $table->timestamps();
      });
      echo "Creating chat relations".PHP_EOL;
      Schema::table('chats', function (Blueprint $table) {
          $table->integer('user_id')->unsigned()->nullable()->after('id');

          $table->foreign('user_id')->references('id')->on('users');
      });
      echo "Creating pin_files relations".PHP_EOL;
      Schema::table('pin_files', function (Blueprint $table) {
          $table->integer('pin_id')->unsigned()->nullable()->after('id');

          $table->foreign('pin_id')->references('id')->on('pines');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}