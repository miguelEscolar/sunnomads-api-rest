<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CountryMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      echo "Creating country table".PHP_EOL;

      Schema::create('countries', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->timestamps();
      });

      echo "Creating our position table".PHP_EOL;
      Schema::create('positions', function (Blueprint $table) {
          $table->increments('id');

          $table->timestamps();
      });
      echo "Creating country and pilot column foreign keys".PHP_EOL;
      Schema::table('retos', function (Blueprint $table) {
          $table->integer('country_id')->unsigned()->nullable()->after('id');
          $table->integer('pilot_id')->unsigned()->nullable()->after('id');

          $table->foreign('country_id')->references('id')->on('countries');
          $table->foreign('pilot_id')->references('id')->on('users');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
