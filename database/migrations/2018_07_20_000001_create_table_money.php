<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      echo "Starting to create money table".PHP_EOL;

        Schema::create('dinero', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dinero');
            $table->integer('milestone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Starting to drop tables".PHP_EOL;

      Schema::dropIfExists('dinero');
    }
}
