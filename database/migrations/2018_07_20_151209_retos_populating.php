<?php

use App\Models\Pin;
use App\Models\Tipo;
use App\Models\Reto;
use App\Models\User;
use App\Models\Estado;
use App\Models\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RetosPopulating extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Todos')->first();
        $user = User::where('email','albecortes@gmail.com')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "No más de 2€ al día";
        $reto->descripcion = "Los pilotos no pueden gastar más de 2€/día por persona. Quedan excluidos del presupuesto tanto el agua como el diésel de la furgoneta.";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Todos')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Nada de hoteles";
        $reto->descripcion = "Los pilotos no pueden pagar por dormir. Deben buscarse la vida todas las noches para encontrar un sitio donde les dejen dormir de forma gratuita o a cambio de algún servicio y si no encuentran nada dormir en tienda de campaña o dentro de la furgoneta.";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Todos')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Nada de GPS";
        $reto->descripcion = "Durante el viaje los pilotos no podrán utilizar ningún tipo de dispositivo GPS propio. Para orientarse deben utilizar sus habilidades como aventureros, el sol, una brújula o un mapa en papel. Vamos como hacían nuestros padres!";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Todos')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "LLevar a la planta PITA y plantarla en un Jardín Mongol";
        $reto->descripcion = "Gracias a nuestra nómada Chiara tenemos el reto de llevar durante todo el viaje y sin que se muera la planta PITA. Una vez llegados a Mongolia tenemos que plantarla en un jardín allí.";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Mongolia')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Macarena en un local Mongol";
        $reto->descripcion = "Nuestros pilotos tendrán que conseguir que un local Mongol baile la macarena. ¿Lo conseguirán?";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Rusia')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Todos de flamencos";
        $reto->descripcion = "Nuestro pilotos no sólo tendrán que vestirse con traje de flamenca, sino que tendrán que deleitarnos con un bailecito hahahaha. Conseguirán que algún Turco se les una?";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Irán')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Cocinar salmorejo andalúz";
        $reto->descripcion = "Durante las jornadas por Irán tendrán que conseguir entrar en una casas para prepararles un salmorejo andaluz. Si lo consiguen esa noche dormirán calentitos!!";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Rusia')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Cruzar un río a nado";
        $reto->descripcion = "Nuestros pilotos tendrán que encontrar un río y cruzarlo a nado. ¿Cómo harán para grabarlo?";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Uzbekistán')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Jota aragonesa";
        $reto->descripcion = "Que los pilotos tendrán que mostrar su habilidad bailando una jota aragonesa cuando estén en Samarcanda!!";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Mongolia')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "Encontrar un chamán";
        $reto->descripcion = "Durante el paso por Mongolia, nuestros pilotos tendrán que encontrar un chamán y, como si esto no fuera suficiente, tendrán que conseguir que les lleve de caza al estilo tradicional. ¿Cómo será?";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Uzbekistán')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "(Vitrum) Artesanos del vidrio";
        $reto->descripcion = "Nuestro sponsor Vitrum nos manda el reto a todos los pilotos de buscar durante el viaje, antes de llegar a Uzbekistán, un artesano del vidrio con el que hacernos una foto.";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Irán')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "(Alazara) Paella en familia";
        $reto->descripcion = "Nuestro sponsor Almazara nos manda el reto a todos los pilotos de cocinar una paella para una familia en Irán. Esta paella la tienen que deleitar al menos dos mujeres y ser cocinada con el fantástico aceite de oliva que ellos comercializan, el 1880.";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Mongolia')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = null;
        $reto->titulo = "(Dental Corbella) Dientes limpios!";
        $reto->descripcion = "Nuestro sponsor dental Corbella nos manda el reto de conseguir que un grupo de niños se laven los dientes a la vez alrededor de una fuente. Tendremos que hacer el reto antes de llegar a Mongolia, lo conseguiremos??";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $pilot = User::where('email','juanmartin1892@gmail.com')->first();
        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Croacia')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = $pilot->id;
        $reto->titulo = "Una falda escocesa para Juanito";
        $reto->descripcion = "Juanito tiene que llevar durante todo el viaje por Croacia una falda escocesa. Qué aireado que va a ir!!!";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $pilot = User::where('email','a.g.miralles@gmail.com')->first();
        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Kazajistán')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = $pilot->id;
        $reto->titulo = "Dormir gracias a Tinder";
        $reto->descripcion = "Durante la estancia en Kazajistán los dedos de Albert tienen que ser muy, pero que muy hábiles y conseguir que uno de sus tinder match les deje un sitio calentito para que todos los pilotos puedan pasar la noche. Será su perfil suficientemente atractivo para las Kazajas??";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $pilot = User::where('email','m.escolar.ituarte@gmail.com')->first();
        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Turquía')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = $pilot->id;
        $reto->titulo = "Raparse el pelo";
        $reto->descripcion = "El pelo da demasiado calor!! Nuestro piloto Miguel ha aceptado el reto de raparse la cabeza durante el paso por Turquía, será capaz de hacerlo??";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

        $pilot = User::where('email','albecortes@gmail.com')->first();
        $estado = Estado::where('slug','aceptado')->first();
        $country = Country::where('name','Rusia')->first();

        $reto = new Reto();
        $reto->country_id = $country->id;
        $reto->estado_id = $estado->id;
        $reto->user_id = $user->id;
        $reto->pilot_id = $pilot->id;
        $reto->titulo = "Tumbar a un ruso";
        $reto->descripcion = "El duro ha sido el deporte oficial en Albacete durante mucho tiempo. Nuestro piloto tendrá que usar sus habilidades aprendidas para tumbar a un Ruso a base de Vodka jugando al duro. El día siguiente va a ser duro!";
        $reto->lugar = null;
        $reto->video = "";
        $reto->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
