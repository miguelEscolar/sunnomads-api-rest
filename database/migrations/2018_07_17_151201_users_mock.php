<?php

use App\Models\User;
use App\Models\Rol;
use App\Models\Ranking;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersMock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        echo "--------------------------------------".PHP_EOL;
        echo "                 WARNING              ".PHP_EOL;
        echo "--------------------------------------".PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        echo "Do not upload this on production has mocked users".PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;

        $rol = Rol::where('slug', 'pilot')->first();

        $authUser = new User;
        $authUser->name = 'Alberto Cortés';
        $authUser->email = 'albecortes@gmail.com';
        $authUser->password = bcrypt('Pilot_albecortes');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();

        $rol = Rol::where('slug', 'pilot')->first();

        $authUser = new User;
        $authUser->name = 'Albert Miralles';
        $authUser->email = 'a.g.miralles@gmail.com';
        $authUser->password = bcrypt('Pilot_a.g.miralles');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();

        $rol = Rol::where('slug', 'pilot')->first();

        $authUser = new User;
        $authUser->name = 'Juan Martín';
        $authUser->email = 'juanmartin1892@gmail.com';
        $authUser->password = bcrypt('Pilot_juanmartin1892');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();

        $rol = Rol::where('slug', 'pilot')->first();

        $authUser = new User;
        $authUser->name = 'Miguel Escolar';
        $authUser->email = 'm.escolar.ituarte@gmail.com';
        $authUser->password = bcrypt('Pilot_m.escolar.ituarte');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();

        $rol = Rol::where('slug', 'admin')->first();

        $authUser = new User;
        $authUser->name = 'test2';
        $authUser->email = 'admin@test.test';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Gloria Torres';
        $authUser->email = 'gtorsa@hotmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();        

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Gregorio Cortés';
        $authUser->email = 'gregoriocortes@hotmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();   

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Maria Miralles';
        $authUser->email = 'mirallesto@gmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();  

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Jeroni Soler';
        $authUser->email = 'jeroni.sg@gmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save();  

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Pau Guerra';
        $authUser->email = 'socunkrac@gmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save(); 

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Marc Turbau Torres';
        $authUser->email = 'marc.turbau@gmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save(); 

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Aran Dominguez Rivero';
        $authUser->email = 'arandr86@hotmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save(); 

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Pau Rourich';
        $authUser->email = 'pau69rs@hotmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save(); 

        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = 'Marina Armero';
        $authUser->email = 'marina.armero@gmail.com';
        $authUser->password = bcrypt('Admin123');
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        $ranking = new Ranking();
        $ranking->user_id = $authUser->id;
        $ranking->puntos = 0;
        $ranking->retos_votados = 0;
        $ranking->save(); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
