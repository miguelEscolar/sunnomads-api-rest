<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Voto;
use App\Models\User;
use App\Models\Ranking;
use App\Models\Reto;

class Puntos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $reto = Reto::where('estado_id', 4)->get();
      foreach($reto as $conseguido) {
        $votos = Voto::where('reto_id', $conseguido->id)->get();
        $control = [];

        foreach($votos as $voto) {
          $pos = array_search($voto->user_id, $control);
          if ($pos === false) {
              array_push($control, $voto->user_id);
              $ranking = Ranking::where('user_id', $voto->user_id)->first();
              $ranking->puntos += 3;
              $ranking->save();
          }
        }

      }

      $reto = Reto::where('estado_id', 5)->get();
      foreach($reto as $fallado) {
        $votos = Voto::where('reto_id', $fallado->id)->get();
        $control = [];
        
        foreach($votos as $voto) {
          $pos = array_search($voto->user_id, $control);
          if ($pos === false) {
              array_push($control, $voto->user_id);
              $ranking = Ranking::where('user_id', $voto->user_id)->first();
              $ranking->puntos += 1;
              $ranking->save();
          }
        }

      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}