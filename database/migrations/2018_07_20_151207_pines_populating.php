<?php

use App\Models\Pin;
use App\Models\Tipo;
use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PinesPopulating extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::where('email','albecortes@gmail.com')->first();
        $tipo = Tipo::where('slug','punto')->first();

        $pin = new Pin();
        $pin->GPS = "41.470244, 2.093152";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();

        $pin = new Pin();
        $pin->GPS = "44.403657, 8.929771";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();

        $pin = new Pin();
        $pin->GPS = "40.999047, 29.055798";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();

        $pin = new Pin();
        $pin->GPS = "40.252865, 58.439690";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();

        $pin = new Pin();
        $pin->GPS = "39.649437, 66.963673";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();

        $pin = new Pin();
        $pin->GPS = "42.469105, 78.402299";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();

        $pin = new Pin();
        $pin->GPS = "43.214636, 76.938665";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();

        $pin = new Pin();
        $pin->GPS = "53.321493, 83.813959";
        $pin->archivo = null;
        $pin->texto_lugar = null;
        $pin->titulo = null;
        $pin->user_id = $user->id;
        $pin->tipo_id = $tipo->id;
        $pin->save();        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
