<?php

use App\Models\Position;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PositionPopulating extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $position = new Position();
      $position->country_id = 1;
      $position->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
