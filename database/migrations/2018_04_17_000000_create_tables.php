<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      echo "Starting to create tables".PHP_EOL;

      echo "Creating users table".PHP_EOL;
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('avatar')->nullable();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->timestamps();
            $table->softDeletes();
        });

        echo "Creating hash table".PHP_EOL;
        Schema::create('hashes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash');
            $table->timestamps();
            $table->softDeletes();
        });

        echo "Creating roles table".PHP_EOL;
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        echo "Creating pines table".PHP_EOL;
        Schema::create('pines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable();
            $table->string('GPS')->nullable();
            $table->string('archivo')->nullable();
            $table->string('texto_lugar')->nullable();
            $table->string('titulo')->nullable();
            $table->timestamps();
        });

        echo "Creating retos table".PHP_EOL;
        Schema::create('retos', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('descripcion')->nullable();
            $table->string('lugar')->nullable();
            $table->string('video')->nullable();
            $table->string('titulo')->nullable();
            $table->timestamps();
        });

        echo "Creating tipos table".PHP_EOL;
        Schema::create('tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        echo "Creating estados table".PHP_EOL;
        Schema::create('estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        echo "Creating ranking table".PHP_EOL;
        Schema::create('rankings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('retos_votados');
            $table->integer('puntos');
            $table->timestamps();
        });

        echo "Creating votos table".PHP_EOL;
        Schema::create('votos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Starting to drop tables".PHP_EOL;

      Schema::dropIfExists('users');
      Schema::dropIfExists('hashes');
      Schema::dropIfExists('roles');
      Schema::dropIfExists('pines');
      Schema::dropIfExists('retos');
      Schema::dropIfExists('tipos');
      Schema::dropIfExists('estados');
      Schema::dropIfExists('rankings');
      Schema::dropIfExists('votos');
    }
}
