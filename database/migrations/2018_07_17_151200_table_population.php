<?php

use App\Models\Tipo;
use App\Models\Rol;
use App\Models\Estado;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePopulation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Starting to populate tables".PHP_EOL;
        $new = new Tipo();
        $new->name = 'foto';
        $new->slug = 'foto';
        $new->save();

        $new = new Tipo();
        $new->name = 'video';
        $new->slug = 'video';
        $new->save();

        $new = new Tipo();
        $new->name = 'confesion';
        $new->slug = 'confesion';
        $new->save();

        $new = new Tipo();
        $new->name = 'averia';
        $new->slug = 'averia';
        $new->save();

        $new = new Tipo();
        $new->name = 'reto';
        $new->slug = 'reto';
        $new->save();

        $new = new Tipo();
        $new->name = 'parada';
        $new->slug = 'parada';
        $new->save();

        $new = new Tipo();
        $new->name = 'punto';
        $new->slug = 'punto';
        $new->save();


        $new = new Rol();
        $new->name = 'piloto';
        $new->slug = 'pilot';
        $new->save();

        $new = new Rol();
        $new->name = 'administrador';
        $new->slug = 'admin';
        $new->save();

        $new = new Rol();
        $new->name = 'jugador';
        $new->slug = 'player';
        $new->save();

        $new = new Estado();
        $new->name = 'pendiente de decision';
        $new->slug = 'pendiente';
        $new->save();

        $new = new Estado();
        $new->name = 'reto aceptado';
        $new->slug = 'aceptado';
        $new->save();

        $new = new Estado();
        $new->name = 'reto rechazado';
        $new->slug = 'rechazado';
        $new->save();

        $new = new Estado();
        $new->name = 'reto conseguido';
        $new->slug = 'conseguido';
        $new->save();

        $new = new Estado();
        $new->name = 'reto fallado';
        $new->slug = 'fallado';
        $new->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
