<?php

use App\Models\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CountryPopulating extends Migration
{

    protected $countries;

    public function __construct()
    {
        $this->countries = [
          'España',
          'Francia',
          'Italia',
          'Eslovenia',
          'Croacia',
          'Serbia',
          'Bulgaria',
          'Turquía',
          'Irán',
          'Turkmenistán',
          'Uzbekistán',
          'Kazajistán',
          'Kirguistán',
          'Rusia',
          'Mongolia',
          'Todos',
          'Cualquiera'];
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('positions', function (Blueprint $table) {
        $table->integer('country_id')->unsigned()->nullable()->after('id');

        $table->foreign('country_id')->references('id')->on('countries');
      });

      echo "Creating country table".PHP_EOL;

      foreach($this->countries as $country) {
        $c = new Country();
        $c->name = $country;
        $c->save();
      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
