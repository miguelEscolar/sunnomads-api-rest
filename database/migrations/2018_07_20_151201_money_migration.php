<?php

use App\Models\Dinero;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoneyMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Money update on database".PHP_EOL;

        $dinero = new Dinero();
        $dinero->dinero = 6311;
        $dinero->milestone = 7000;
        $dinero->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      echo "Please do a manual down".PHP_EOL;
    }
}
