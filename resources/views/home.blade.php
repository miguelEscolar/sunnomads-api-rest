<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Bienvenido a la app de sunnomads para seguir nuestro viaje</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sunnomad es una ONG que colabora con países en desarrollo llevándoles energía solar a través de una aventura solidaria. Gracias a las energías renovables podemos proveer de electricidad a aquellas zonas donde no las hay.">
        <meta name="keywords" content="energía solar,proyecto solidario,colaborar,ONG,Renovables,Aventura">
        <meta name="author" content="Sunnomad">

        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Bootstrap -->
        <link href="//stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="//use.fontawesome.com/releases/v5.1.1/css/all.css" crossorigin="anonymous" rel="stylesheet">

        <link href="./css/style.css" rel="stylesheet">

        <script src="//code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <!-- Jquery cookies -->
        <script src="//cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
        <!-- Bootstrap tables -->
        <!-- Latest compiled and minified CSS -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css" rel="stylesheet">
        <!-- Latest compiled and minified JavaScript -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
        <!-- Latest compiled and minified Locales -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-es-ES.min.js"></script>

        <!-- Para formatear y trabajar con fechas -->
        <script src="//www.datejs.com/build/date.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-2 order-xl-1 col-12 order-2">
                    <div class="row justify-content-center">
                        <!-- AVATAR -->
                        <div class="col-xl-12 mt-2 user-info">
                            <div class="text-center">
                                <h4>Bienvenido</h4>
                                <p id="avatar-mail" class="avatar-mail">cargando...</p>
                                <div class="move-logout-right">
                                    <i class="fa fa-sign-out-alt" onclick="logOut()"></i>
                                </div>
                            </div>
                        </div>                    
                        <!-- Tu posición -->
                        <div id="ranking" style="display: none;" class="col-xl-12 col-md-5 ml-2 mt-2 tile-stats">
                            <div class="icon"><i class="medal fa fa-medal"></i></div>
                            <div class="count position-rank">0</div>
                            <h3>Tu posición</h3>
                            <p>Conseguida con <span class="position-points">0</span> puntos</p>
                        </div>

                        <!-- Retos por validar -->
                        <div id="pendingRetos" style="display: none;" class="col-xl-12 col-md-5 ml-2 mt-2 x_panel">
                            <div class="x_title">
                                <h2>Los retos por validar son:</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table class="table" id="tablePendingRetos"></table>
                            </div>
                        </div>

                        <!-- Próximo reto -->
                        <div id="nextReto" style="display: none;" class="col-xl-12 col-md-5 ml-2 mt-2 x_panel">
                            <div class="x_title">
                                <h2>El próximo reto es:</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <ul class="list-unstyled timeline widget">
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <h2 class="title">
                                                        <a class="NextRetoTitle"></a>
                                                    </h2>
                                                    <div class="byline">
                                                        <span>Dirigido a <span class="NextRetoPerson"></a></span>
                                                    </div>
                                                    <p class="NextRetoExp excerpt"></p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="votacion mx-auto">
                                        <button type="button" class="btn-voto btn btn-outline-link btn-sm votoPosJS" data-voto="1" data-idReto=""><i class="fa fa-thumbs-up"></i> Podrán</button>
                                        <button type="button" class="btn-voto btn btn-outline-link btn-sm votoNegJS" data-voto="0" data-idReto=""><i class="fa fa-thumbs-down"></i> No podrán</button>
                                    </div>
                                </div>
                                <p class="sinVotar" style="display: none;"></p>
                            </div>
                        </div>
                        <!-- Dona a sunNomads -->
                        <div class="col-xl-12 col-md-10 ml-2 mt-2 x_panel donations">
                            <div class="x_title">
                                <h2>Participa y dona:</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="row justify-content-center align-items-center">
                                        <div class="col-xl-12 col-sm-8 order-sm-2 text-center">
                                            <p>Gracias a la aportación de todos ya hemos conseguido <span class="donation-value font-weight-bold">0€</span>, ayúdanos a llegar a nuestro objetivo de 15.000€!</p>
                                        </div>
                                        <div class="col-xl-6 col-sm-4 order-sm-1 p-0 m-0 text-center">
                                            <div class="chart" data-percent="0">
                                                <span class="donation-percentage font-weight-bold">0%</span>
                                            </div>
                                            <form action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post" target="_top">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="J4L5MXJRAAR8U">
                                                <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal, la forma rápida y segura de pagar en Internet.">
                                                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Ir a crear pin -->
                        <div id="createPin" class="col-xl-12 col-md-5 ml-2 mt-2 pt-2 tile-stats text-center" style="display:none;">
                            <div class="row">
                                <div class="col-4">
                                    <a href="/addpin">
                                        <img width="62px" src="./img/gps.png">
                                    </a>
                                </div>
                                <div class="col-8 pt-3 pb-3">
                                    <p>Haz click aquí para crear un nuevo pin en el mapa del viaje</p>
                                </div>
                            </div>
                        </div>       
                        <!-- Cambiar recaudado -->
                        <div id="CambiarPasta" class="col-xl-12 col-md-5 ml-2 mt-2 pt-2 tile-stats text-center" style="display:none;">
                            <div class="row">
                                <div class="col-4">
                                    <a href="/changeMoney">
                                        <img width="62px" src="./img/piggybank.png">
                                    </a>
                                </div>
                                <div class="col-8 pt-3 pb-3">
                                    <p>Haz click aquí para cambiar el total recaudado por sunNomads</p>
                                </div>
                            </div>
                        </div> 
                        <!-- Comparte en redes -->
                        <div id="RedesFB" class="col-xl-12 col-md-5 ml-2 mt-2 pt-2 tile-stats text-center">
                            <p>¡Tu ayuda es fundamental!, ¡Ayúdanos a darle visibiliad al proyecto! </p>
                            <p><strong>Haz click en compartir!</strong></p>
                            <a href="javascript: void(0);" data-layout="button" onclick="window.open('https://www.facebook.com/sharer.php?u=app.sunnomads.com&summary=No+te+pierdas+el+viaje+m%C3%A1s+loco+del+2018&title=Viaje+a+Mongolia+de+sunNomads&description=Viajaremos+14.000km+con+una+furgoneta+de+35+a%C3%B1os+y+superaremos+tus+retos+para+para+conseguir+fondos+para+llevar+electricidad+a+Madagascar&picture=https://app.sunnomads.com/img/mapa_viaje.png','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                <img class="FB-share mt-2 mb-2" src="https://www.dropbox.com/s/gmvh315jf1lz81n/FB-share.png?raw=1">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-10 order-xl-2 col-12 order-1 p-1">
                    <div class="chatContainer shadowChat">
                        <span class="fa-stack fa-2x">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fas fa-comments fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div class="msgSinLeer"></div>           
                    <div class="container-fluid mapSection" id="mapSection">
                        <div class="mapChallenges" id="mapChallenges"></div>
                    </div>
                </div>

            </div>
            <div class="row mt-4">
                <!-- Listado de participantes y puntuación -->
                <div class="col-xl-6 mt-2">
                    <div class="x_panel">
                        <div class="x_title font">
                            <h2 class="font-weight-bold">Listado de participantes y puntuaciones:</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="alert alert-info alertParticipants" role="alert">
                                <button type="button" class="close closeParticipants" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>                                
                                <div class="justify-content-center">
                                    <i class="fa fa-info-circle fa-3x float-left mr-4 mb-4"></i>
                                    <p>Vota nuestros retos y consigue 1 punto si no lo aciertas o 3 puntos si lo aciertas. </p>
                                    <p>Recuerda que al final del viaje, los 3 participantes que más votos consigan podrán acompañarnos a Madagascar para ser testigos de cómo realizamos las 3 instalaciones.</p>
                                </div>
                            </div>
                            <div class="dashboard-widget-content">
                                <table class="table" id="tableParticipantes"></table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Listado de retos y votos -->
                <div class="col-xl-6 mt-2">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 class="font-weight-bold">Listado de retos: <small class="sinVotar" style="display: none;"></small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="alert alert-info alertRetos" role="alert">
                                <button type="button" class="close closeRetos" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>                                
                                <div class="justify-content-center">
                                    <i class="fa fa-info-circle fa-3x float-left mr-4 mb-4"></i>
                                    <p>Entre todos podemos hacer el viaje más divertido (o difícil).</p>
                                    <p>Puedes proponer a los pilotos el reto que quieras, nuestro equipo de nómadas lo evaluará y si no nos hace entrar en la cárcel lo aceptaremos.</p>
                                </div>
                            </div>                            
                            <div class="dashboard-widget-content">
                                <table class="table" id="tableRetos"></table>
                                <!-- Déjaos un reto -->
                                <div class="col-xl-12 ml-2 mt-2 pt-2 tile-stats text-center">
                                    <h6 class="mb-3 mt-4">¿Quieres proponernos un nuevo reto?</h6>
                                    <a class="btn-crear-reto btn btn-primary active mb-4" style="display:none;" role="button">Proponer un reto</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Crear reto -->
        <div class="modal fade" id="modalNuevoReto" tabindex="-1" role="dialog" aria-labelledby="modalNuevoRetoTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalNuevoRetoTitle">Aquí puedes crear un reto para los pilotos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="retoTitle">Pon un título al reto</label>
                            <input type="text" class="form-control form-control-sm" id="retoTitle" placeholder="Escribe el título del reto">
                        </div>
                        <div class="form-group">
                            <label for="retoDesc">Explícanos el reto que tienes en mente</label>
                            <textarea class="form-control form-control-sm" id="retoDesc" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="retoWho">¿A quién quieres retar?</label>
                            <select class="form-control form-control-sm" id="retoWho">
                              <option value="0">A todos lo pilotos</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="retoWhere">¿En qué país quieres que se haga?</label>
                            <select class="form-control form-control-sm" id="retoWhere">
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button id="createReto" type="button" class="btn btn-primary">Crear reto</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Gracias por crear un reto -->
        <div class="modal fade" id="modalNuevoRetoOK" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                          <h5 class="pb-3">Gracias por enviarnos este reto!!</h5>
                          <span class="modal-body-text pb-4">Ya está en manos de nuestro equipo de nómadas para ser validado</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="chatModal" class="modal modeless" tabindex="-1" role="dialog" data-backdrop="false" data-focus="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">   
                    <div class="modal-body">
                        <div class="chatContainerTexts">
                            <ul>
                                <li><i class="fas fa-spinner fa-spin mr-2"></i> Cargando chat...</li>
                            </ul>
                        </div>   
                        <div class="BtnChat input-group mt-3">
                          <textarea id="msgToSend" class="form-control form-control-sm" placeholder="Envia un mensaje"></textarea>
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" id="enviaChat"><i class="fab fa-telegram-plane"></i></button>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="connecting-server p-4" style="display:none;">
            <span class="pl-4 pr-4"><i class="fa fa-spinner fa-3x fa-spin mr-3"></i>Conectando al servidor...</span>
        </div>

        <div class="error-alert p-4" style="display:none;">
            <span class="pl-4 pr-4"></span>
        </div>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAee-d2JIYOk7Y4WmRPgijWI62Cn9uH7Ec" async defer></script>
        <script type="text/javascript" src="https://rendro.github.io/easy-pie-chart/javascripts/jquery.easy-pie-chart.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/home.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/googleMap.js" charset="utf-8"></script>
        <div id="fb-root"></div>
        <script>
            (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.0&appId=1169908299691013&autoLogAppEvents=1';
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>
