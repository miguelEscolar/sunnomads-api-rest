<!doctype html>
<html>
    <head>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            img {
              width: 200px
            }
            h3 {
                margin-bottom: 1px;
            }
            .contentMail {
                padding: 30px; 
                display: flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
            }
            .title {
                font-family: 'Raleway', sans-serif;
                color: #00bac9;
                font-size: 33px;
                line-height: 35px;
            }
            .text {
                color: #0a386e;
                font-weight: normal;
                margin-bottom: 0px;
                line-height: 1.5em;
            }
            .textBold {
                font-family: 'Raleway', sans-serif;
                font-weight: bold;
                margin-bottom: 30px;
                line-height: 1.5em;
            }
            a {
                color: #00bac9;
                font-weight: 900;
                text-decoration: none;
            }
            .colorLight {
                color: #00bac9;
            }

        </style>
    </head>
    <body>
        <table class="contentMail">
            <tr>
                <td>
                    <img src="https://www.sunnomads.com/img/logo-sunnomads.png" alt="">
                </td>
            </tr>
            <tr>
                <td>
                    <h3 class="title">{{$user->name}}, bienvenido a sunNomads!</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="textBold">Gracias por registrarte en nuestra app para seguir nuestro viaje Mongolia.</p>
                    <p class="text">No olvides que puedes votar y proponernos cualquier reto!, para ello haz click<a href = "www.sunnomads.com"> aquí </a></p>
                    <p class="text">Intentaremos pasar un buen rato juntos con el objetivo de llevar electricidad donde no la hay.</p>
                </td>
            </tr>
        </table>        
    </body>
</html>
