<!doctype html>
<html>
    <head>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            img {
              width: 200px
            }
            h3 {
                margin-bottom: 1px;
            }
            .contentMail {
                padding: 30px; 
                display: flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
            }
            .title {
                font-family: 'Raleway', sans-serif;
                color: #00bac9;
                font-size: 33px;
                line-height: 35px;
            }
            .text {
                color: #0a386e;
                font-weight: normal;
                margin-bottom: 0px;
                line-height: 1.5em;
            }
            .textBold {
                font-family: 'Raleway', sans-serif;
                font-weight: bold;
                margin-bottom: 30px;
                line-height: 1.5em;
            }
            a {
                color: #00bac9;
                font-weight: 900;
                text-decoration: none;
            }
            .colorLight {
                color: #00bac9;
            }

        </style>
    </head>
    <body>
        <table class="contentMail">
            <tr>
                <td>
                    <img src="https://www.sunnomads.com/img/logo-sunnomads.png" alt="">
                </td>
            </tr>
            <tr>
                <td>
                    <h3 class="title">Estamos muy contentos</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="textBold">Hemos llegado a un nuevo objetivo en nuestra recaudación, ya tenemos <span class="colorLight">{{$dinero}} €</span></p>
                    <p class="text">Cada dias estamos mas cerca de alcanzar nuesto objetivo de 15.000€. Con este dinero podremos hacer, en Madagascar, tres instalaciones solares que suman 7kW y así llevar electricidad a escuelas infantiles</p>
                    <p class="text">Puedes seguir la evolución en <a href = "www.sunnomads.com"> www.sunnomads.com </a></p>
                    <p class="text">Si no eres de los que ha donado, aún estás a tiempo :)</p>
                </td>
            </tr>
        </table>          
    </body>
</html>
