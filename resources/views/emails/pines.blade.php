<!doctype html>
<html>
    <head>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            img {
              width: 200px
            }
            h3 {
                margin-bottom: 1px;
            }
            h4 {
                margin-bottom: 5px;
            }
            .contentMail {
                padding: 30px; 
                display: flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
            }
            .title {
                font-family: 'Raleway', sans-serif;
                color: #00bac9;
                font-size: 33px;
                line-height: 35px;
            }
            .text {
                color: #0a386e;
                font-weight: normal;
                margin-bottom: 0px;
                line-height: 1.5em;
            }
            .textBold {
                font-family: 'Raleway', sans-serif;
                font-weight: bold;
                margin-bottom: 30px;
                line-height: 1.5em;
            }
            a {
                color: #00bac9;
                font-weight: 900;
                text-decoration: none;
            }
            .colorLight {
                color: #00bac9;
            }
            .cardpin {
              display: flex;
              align-items: start;
              flex-direction: column;
              text-align: left;
              margin-bottom: 10px;
            }
            ul {
              list-style-type: disc;
              list-style-position: outside;
            }

        </style>
    </head>
    <body>
        <table class="contentMail">
            <tr>
                <td>
                    <img src="https://www.sunnomads.com/img/logo-sunnomads.png" alt="">
                </td>
            </tr>
            <tr>
                <td>
                    <h3 class="title">No te olvides de seguir nuestra aventura</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="textBold">Estas son nuestras ultimas actualizaciones</p>
                    <ul>
                      @foreach ($pines as $pin)
                          <li class="cardpin">
                              <h4 class="colorLight"> {{$pin->title}} </h4><small><i> {{$pin->created_at}} </i></small> 
                              <p>{{$pin->descripcion}}</p>
                          </li>
                      @endforeach 
                    </ul>
                    <p class="text">¡Si esperas te lo pierdes! Entra<a href = "https://app.sunnomads.com"> aquí </a>para seguir el viaje.</p>
                </td>
            </tr>
        </table>          
    </body>
</html>


