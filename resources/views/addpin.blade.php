<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"   integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <link rel="stylesheet" href="./css/style.css">

    </head>
    <body>
      <div class="container">
        <div class="card card-container">
              <img id="profile-img" class="profile-img-card" src="./img/logo-sunnomads.png" />
              <p id="profile-name" class="profile-name-card"></p>
              <form id="loginForm" name="loginForm" action="#" class="form-signin" onsubmit="createPin(this); return false;">
                  <span id="reauth-email" class="reauth-email"></span>

                  <div class="form-group">
                      <label for="inputTitulo">Introduce un título</label>
                      <input type="text" id="inputTitulo" name="inputTitulo" class="form-control form-control-sm" placeholder="Titulo" required autofocus>
                  </div>
                  <div class="form-group">
                      <label for="inputDescription">Qué texto quieres que la gente vea</label>
                      <textarea id="inputDescription" name="inputDescription" class="form-control form-control-sm" placeholder="Descripcion" required></textarea>
                  </div>
                  <div class="form-group">
                      <label for="inputGps">Éstas son tus coordenadas GPS</label>
                      <input type="text" id="inputGps" name="inputGps" class="form-control form-control-sm" placeholder="Cargando..." required>
                  </div>        
                  <div id="GroupArchivoUrl" class="form-group">
                      <label for="inputArchivoUrl">Pon la url del archivo</label>
                      <div class="input-group mb-3">
                        <input type="text" id="inputArchivoUrl" name="inputArchivoUrl" class="form-control form-control-sm" placeholder="Url al archivo" aria-label="Url al archivo">
                        <div class="input-group-append">
                          <button class="btn btn-outline-primary btn-sm" type="button" id="AddNewFile">+</button>
                        </div>
                      </div>
                  </div>                    
                  <div class="form-group">
                      <label for="inputTextoLugar">Describe el lugar donde estás</label>
                      <textarea id="inputTextoLugar" name="inputTextoLugar" class="form-control form-control-sm" placeholder="Texto Lugar"></textarea>
                  </div>                     
                  <div class="form-group">
                      <label for="inputTextoLugar">Describe el lugar donde estás</label>
                      <select class="form-control form-control-sm" id="inputType" name="inputType">
                          <option value="1" selected>Foto</option>
                          <option value="2">Video</option>
                          <option value="3">Confesion</option>
                          <option value="4">Averia</option>
                          <option value="5">Reto</option>
                          <option value="6">Parada</option>
                          <option value="7">Punto en mapa</option>
                      </select>
                  </div>                    
                  
                  <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Añadir pin</button>
                  </form><!-- /form -->
            </div><!-- /card-container -->
        </div><!-- /container -->
        <script src="./js/addPin.js" charset="utf-8"></script>
    </body>
</html>
