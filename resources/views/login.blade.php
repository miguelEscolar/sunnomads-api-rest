<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"   integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <link href="./css/style.css" rel="stylesheet">

    </head>
    <body>
      <div class="container">
        <div class="card card-container">
          <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
              <img id="profile-img" class="profile-img-card" src="./img/logo-sunnomads.png" />
              <p id="profile-name" class="profile-name-card"></p>
              <form name="loginForm" action="#" class="form-signin" onsubmit="login(this); return false;">
                  <span id="reauth-email" class="reauth-email"></span>

                  <div class="form-group">
                      <label for="inputEmail">Escribe tu email</label>
                      <input type="email" class="form-control form-control-sm" id="inputEmail"  name="inputEmail" placeholder="dirección de email" required autofocus>
                  </div>
                  <div class="form-group">
                      <label for="inputPassword">Escribe tu contraseña</label>
                      <input type="password" class="form-control form-control-sm" id="inputPassword"  name="inputPassword" placeholder="contraseña" required autofocus>
                  </div>

                  <button class="btn btn-primary btn-block btn-signin" type="submit">Sign in</button>
                  </form><!-- /form -->
                  <a href="/register" class="forgot-password mt-3">
                      Si no tienes cuenta, creáte una aqui.
                  </a>
            </div><!-- /card-container -->
        </div><!-- /container -->
        <script src="./js/login.js" charset="utf-8"></script>
    </body>
</html>
