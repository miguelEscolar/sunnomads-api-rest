<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"   integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <link rel="stylesheet" href="./css/style.css">

    </head>
    <body>
      <div class="container">
        <div class="card card-container">
              <img id="profile-img" class="profile-img-card" src="./img/logo-sunnomads.png" />
              <p id="profile-name" class="profile-name-card"></p>
              <form name="loginForm" action="#" class="form-signin" onsubmit="modificarRecoudacion(this); return false;">
                  <div class="form-group">
                      <label for="inputTitulo">Introduce la cantidad recaudada</label>
                      <input type="number" id="inputRecaudación" name="inputRecaudación" class="form-control form-control-sm" placeholder="Recaudación" required autofocus>
                  </div>
                  <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Modificar recaudación</button>
                  </form><!-- /form -->
            </div><!-- /card-container -->
        </div><!-- /container -->
        <script src="./js/changeMoney.js" charset="utf-8"></script>
    </body>
</html>
