<?php

namespace App\Services;

use App\Models\User;
use App\Models\Voto;
use App\Models\Reto;
use App\Models\Estado;
use App\Models\Ranking;
use Illuminate\Support\Facades\DB;

class UserService extends Service
{
    public function editUser($user, $data)
    {
        DB::transaction(function () use ($user, $data) {
            foreach ($data as $clave => $valor) {
              if($clave === 'email' || $clave === 'password') {
                $user->$clave = $valor;
                $user->save();
              }
            }
        });

        return $user;
    }

    public function vote($vote, $reto, $user)
    {
        $estado = Estado::where('slug', $vote)->first();
        if(!$estado) {
          return false;
        }
        $voto = Voto::where('reto_id', $reto->id)
                      ->where('user_id', $user->id)
                      ->first();
        if(!$voto) {
          $voto = new Voto();
          $voto->user_id = $user->id;
          $voto->estado_id = $estado->id;
          $voto->reto_id = $reto->id;
          $voto->save();

          $ranking = Ranking::where('user_id', $user->id)->first();
          if(!$ranking) {
            $ranking = new Ranking();
            $ranking->retos_votados = 0;
            $ranking->puntos = 0;
            $ranking->user_id = $user->id;
            $ranking->save();
          }
          $ranking->retos_votados += 1;
          $ranking->save();
        } else {
          $voto->estado_id = $estado->id;
          $voto->save();
        }

        return $voto;
    }

    public function sinVotar($user)
    {
      $estado = Estado::where('slug', 'aceptado')->first();

      $retosPosibles = Reto::where('estado_id',$estado->id)->get()->pluck('id');
      $votos = Voto::whereIn('reto_id',$retosPosibles)
                    ->where('user_id',$user->id)
                    ->get()->pluck('id');

      return (($retosPosibles->count())-($votos->count()));
    }

}
