<?php

namespace App\Services;

use App\Models\User;
use App\Models\Voto;
use App\Models\Estado;
use App\Models\Reto;
use App\Models\Ranking;
use App\Mail\Acierto;
use App\Mail\Fallo;
use App\Mail\RetoAceptado;
use App\Mail\RetoDenegado;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class RetoService extends Service
{
    public function editReto($user, $data, $reto = false)
    {
      $reto = DB::transaction(function () use ($user, $data, $reto) {
          if ($reto) {
            $reto = Reto::where('id', $reto)->first();
          } else {
            $reto = new Reto();
            $reto->user_id = $user->id;
            $reto->estado_id = 1;
          }
          foreach ($data as $clave => $valor) {
              $reto->$clave = $valor;
          }

          $reto->save();
          return $reto;
      });

      return $reto;
    }

    public function cambiarEstado($estado, $reto)
    {
      $control = false;
      if ($estado === 'conseguido') {
        $control = 'fallado';
      }
      if ($estado === 'fallado') {
        $control = 'conseguido';
      }
      if ($estado === 'aceptado') {
        $user = User::where('id', $reto->user_id)->first();
        Mail::to($user)
        ->send(new RetoAceptado($reto));
      }
      if ($estado === 'rechazado') {
        $user = User::where('id', $reto->user_id)->first();
        Mail::to($user)
        ->send(new RetoDenegado($reto));
      }
      $estado = Estado::where('slug',$estado)->first();

        if($control){

          $votosAcertados = Voto::where('reto_id', $reto->id)
                  ->where('estado_id', $estado->id)
                  ->get();
          if($votosAcertados->count() > 0) {
            $this->puntos($votosAcertados, 3, $control);
          }

          $fallados = Estado::where('slug',$control)->first();
          $votosAcertados = Voto::where('reto_id', $reto->id)
                  ->where('estado_id', $fallados->id)
                  ->get();
          if($votosAcertados->count() > 0) {
            $this->puntos($votosAcertados, 1, $control);
          }
        }
          $reto->estado_id = $estado->id;
          $reto->save();

      return $reto;
    }

    public function puntos($votosAcertados, $puntos, $control) {
      foreach($votosAcertados as $correctos) {
          $ranking = Ranking::where('user_id', $correctos->user_id)->first();
          $ranking->puntos += $puntos;
          $ranking->save();

          $user = User::where('id',$correctos->user_id)->first();
          $posicion = Ranking::where('puntos','>',$ranking->puntos)->get()->count()+1;

          $reto = Reto::where('id',$correctos->reto_id)->first();
      }
      $users = User::whereIn('id',$votosAcertados->pluck('user_id'))->get();
      if ($puntos === 1) {
          Mail::to('hola@sunnomads.com')
            ->bcc($users)
            ->send(new Fallo($reto, $control));
      } else {
          Mail::to('hola@sunnomads.com')
            ->bcc($users)
            ->send(new Acierto($reto, $ranking->puntos, $posicion));
      }
    }
}
