<?php

namespace App\Services;

use App\Models\User;
use App\Models\Pin;
use Illuminate\Support\Facades\DB;

class PinService extends Service
{
    public function editPin($user, $data, $pin = false)
    {
      $pin = DB::transaction(function () use ($user, $data, $pin) {
          if ($pin) {
            $pin = Pin::where('id', $pin)->first();
          } else {
            $pin = new Pin();
            $pin->user_id = $user->id;
          }
          foreach ($data as $clave => $valor) {
              $pin->$clave = $valor;
          }

          $pin->save();
          return $pin;
      });

      return $pin;
    }
}
