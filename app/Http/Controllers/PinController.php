<?php namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Pin;
use App\Models\PinFile;
use App\Services\PinService;
use Illuminate\Http\Request;

class PinController extends Controller
{
    protected $pinService;

    public function __construct(PinService $pinService) {
        $this->pinService = $pinService;
    }

    public function createPin(Request $request)
    {
        $user = $request->user();
        $data = $request->input('data');
        if (!$user->esPiloto()) {
          return response()->json(
              ['error' => 'solo los pilotos pueden editar']
              , 403);
        }
        $pin = $this->pinService->editPin($user, $data);

        return response()->json([
            'pin' => $pin,
        ], 200);
    }
    public function editPin(Request $request, $pin)
    {
        $user = $request->user();
        if (!$user->esPiloto()) {
          return response()->json(
              ['error' => 'solo los pilotos pueden editar']
              , 403);
        }
        $data = $request->input('data');

        $pin  = $this->pinService->editPin($user, $data, $pin);

        return response()->json([
            'pin' => $pin,
        ], 200);
    }

    public function deletePin(Request $request, $pin)
    {
        $user = $request->user();
        if (!$user->esSunnomad()) {
          return response()->json(
              ['error' => 'solo los miembros de sunnomads pueden borrar']
              , 403);
        }
        $pin = Pin::where('id', $pin)->first();
        $pin->delete();

        return response()->json([
            'success' => true,
        ], 200);
    }

    public function getPines(Request $request)
    {
      $user = $request->user();
      $pines = Pin::with('tipo')->get();

      foreach($pines as $pin) {
        $archive = PinFile::where('pin_id', $pin->id)->get();
        $files = [$pin->archivo];
        if($archive->count()>0) {
          $files = [$pin->archivo];
          foreach($archive as $archivo) {
            array_push($files, $archivo->archivo);
          }
        }
        $pin->archivos = $files;
      }
      return response()->json([
          'pines' => $pines,
      ], 200);
    }

    public function getPin(Request $request, $pin)
    {
      $user = $request->user();
      $pin = Pin::where('id', $pin)->with('tipo')->first();

      return response()->json([
          'pin' => $pin,
      ], 200);
    }

    public function addFiles(Request $request, $pin)
    {
      $user = $request->user();
      $pin = Pin::where('id', $pin)->first();
      if (!$user->esPiloto()) {
          return response()->json(
              ['error' => 'solo los pilotos pueden editar']
              , 403);
      }
      if (!$pin) {
          return response()->json(
              ['error' => 'Pin no encontrado']
              , 404);
        }
      $urls = $request->all();

      foreach ($urls as $url) {
        $pinFile = new PinFile();
        $pinFile->archivo = $url;
        $pinFile->pin_id = $pin->id;
        $pinFile->save();
      }

      return response()->json([
          'pin' => $pin,
      ], 200);
    }
}
