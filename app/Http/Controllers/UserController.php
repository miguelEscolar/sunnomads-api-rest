<?php namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Reto;
use App\Models\Voto;
use App\Models\Chat;
use App\Models\Rol;
use App\Models\Dinero;
use App\Models\Ranking;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function editUser(Request $request)
    {
        $user = $request->user();
        $data = $request->input('data');

        $user = $this->userService->editUser($user, $data);

        return response()->json([
            'user' => $user,
        ], 200);
    }

    public function getUser(Request $request)
    {
        $user = $request->user();
        $user = User::where('id', $user->id)->with('rol')->first();
        if($user->esJugador()) {
          $puntos = Ranking::where('user_id', $user->id)->first();
          if($puntos) {
            $user->puntos = $puntos->puntos;
          } else {
            $user->puntos = 0;
          }
          $user->sinVotar = $this->userService->sinVotar($user);
          $mine = Ranking::where('user_id', $user->id)
                  ->first();
          if($mine) {
            $user->puntos = $mine->puntos;
            $user->posicion = Ranking::where('puntos','>',$mine->puntos)->get()->count()+1;
          } else {
            $user->puntos = null;
            $user->posicion = null;
          }
        }
        return response()->json([
            'user' => $user,
        ], 200);
    }

    public function getPilots(Request $request)
    {
        $user = $request->user();

        $rol = Rol::where('slug','pilot')->first();
        $pilots = User::where('rol_id', $rol->id)->get();

        return response()->json([
            'pilots' => $pilots,
        ], 200);
    }

    public function vote(Request $request, $reto)
    {
        $user = $request->user();

        if(!$user->esJugador()) {
          return response()->json(
              ['error' => 'solo los jugadores pueden votar']
              , 403);
        }
        $reto = Reto::where('id', $reto)->first();
        if(!$reto) {
          return response()->json(
              ['error' => 'reto no encontrado']
              , 404);
        }
        if ($reto->estado->slug === 'conseguido' || $reto->estado->slug === 'fallado') {
          return response()->json(
              ['error' => 'Ya no se puede votar este reto.']
              , 403);
        }

        $vote = $request->input('vote');
        $success = $this->userService->vote($vote, $reto, $user);

        return response()->json([
            'success' => $success,
        ], $success?200:400);

    }

    public function getRanking(Request $request)
    {
      $user = $request->user();

      $top = Ranking::orderBy('puntos', 'desc')
              ->orderBy('retos_votados', 'desc')
              ->with('user')
              ->get();

      $mine = Ranking::where('user_id', $user->id)
              ->first();

      return response()->json([
          'top' => $top,
          'mine' => $mine
      ], 200);
    }

    public function getMoney()
    {
      $dinero = Dinero::where('id',1)->first()->pluck('dinero');

      return response()->json([
          'dinero' => $dinero
      ], 200);
    }

    public function saveMoney(Request $request)
    {
      $user = $request->user();
      $recaudacion = $request->input('dinero');
      if (!$user->esSunnomad()) {
        return response()->json(
            ['error' => 'solo los miembros de sunnomads pueden modificar']
            , 403);
      }
      $dinero = Dinero::where('id',1)->first();
      if($dinero->milestone < $recaudacion) {
        //enviar email
      }

      $dinero->dinero = $recaudacion;
      $dinero->save();

      return response()->json([
          'dinero' => $dinero
      ], 200);
    }

    public function addComment(Request $request)
    { 
      $user = $request->user();
      $msg = $request->input('msg');

      if(!$msg) {
          return response()->json(
              ['error' => 'No se pueden guardar mensajes vacios']
              , 400);
      }

      $chat = new Chat();
      $chat->msg = $msg;
      $chat->user_id = $user->id;
      $chat->save();

      return response()->json([
          'chat' => $chat
      ], 200);
    }

    public function getComments(Request $request)
    {
      $user = $request->user();

      $chatMessage = Chat::latest()->limit(20)->get();
      $chat = $chatMessage->reverse()->values();

      foreach ($chat as $comment) {
        $comment->user_name = User::where('id',$comment->user_id)->first()->name;
      }

      return response()->json([
          'chat' => $chat
      ], 200);
    }

}
