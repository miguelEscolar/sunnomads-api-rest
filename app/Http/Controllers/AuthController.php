<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Rol;
use App\Models\Hash;
use App\Mail\Welcome;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginEmail(Request $request)
    {
        $authUser = User::where('email', $request->input('email'))
            ->first();

        if ($authUser) {
            $this->validateLogin($request);
            if ($this->attemptLogin($request)) {
                $token = JWTAuth::fromUser($authUser);
            } else {
                return response()->json(
                    ['error' => 'bad credentials']
                    , 400);
            }
        } else {
          return response()->json(
              ['error' => 'User not found']
              , 403);
        }

        return response()->json(
            ['token' => $token]
            , 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerEmail(Request $request)
    {

        $this->validator($request->all())->validate();
        $authUser = $this->createUser($request);
        $token = JWTAuth::fromUser($authUser);

        Mail::to($authUser)
        ->send(new Welcome($authUser));

        return response()->json(
            ['token' => $token]
            , 200);
    }


    public function changePassword(Request $request)
    {
        $user = $request->user();

        $this->validateLogin($request);
        if ($this->attemptLogin($request)) {
            $this->validatorNewPassword($request->all())->validate();
            $userToModify = User::where('email', $request->input('email'))
                ->first();
            if ($user != $userToModify) {
                return response()->json(['error' => 'You cannot change other user login'], 403);
            }
            $user->password = bcrypt($request->input('new_password'));
            $user->save();
        } else {
            return response()->json(
                ['error' => 'bad credentials']
                , 403);
        }

        return response()->json(
            ['success' => true]
            , 200);
    }

    public function changeEmail(Request $request)
    {
        $user = $request->user();

        $this->validateLogin($request);
        if ($this->attemptLogin($request)) {
            $this->validatorNewEmail($request->all())->validate();
            $userToModify = User::where('email', $request->input('email'))
                ->first();
            if ($user != $userToModify) {
                return response()->json(['error' => 'You cannot change other user login'], 403);
            }
            $user->email = $request->input('new_email');
            $user->save();
        } else {
            return response()->json(
                ['error' => 'bad credentials']
                , 403);
        }

        return response()->json(
            ['success' => true]
            , 200);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nameUser' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:6|same:repeatPassword',
        ]);
    }

    protected function validatorNewPassword(array $data)
    {
        return Validator::make($data, [
            'new_password' => 'required|string|min:6|same:new_password_repeat'
        ]);
    }

    protected function validatorNewEmail(array $data)
    {
        return Validator::make($data, [
            'new_email' => 'required|string|email|max:255|unique:users,email',
        ]);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request)
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * @param Request $request
     * @param $company
     * @return User
     */
    protected function createUser(Request $request)
    {
        $rol = Rol::where('slug', 'player')->first();

        $authUser = new User;
        $authUser->name = $request->input('nameUser');
        $authUser->email = $request->input('email');
        $authUser->password = bcrypt($request->input('password'));
        $authUser->avatar = null;
        $authUser->rol_id = $rol->id;
        $authUser->save();

        return $authUser;
    }
}
