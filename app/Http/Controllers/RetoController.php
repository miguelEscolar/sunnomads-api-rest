<?php namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Reto;
use App\Models\Estado;
use App\Models\Position;
use App\Models\Country;
use App\Models\Voto;
use App\Services\RetoService;
use Illuminate\Http\Request;

class RetoController extends Controller
{
    protected $retoService;

    public function __construct(RetoService $retoService) {
        $this->retoService = $retoService;
    }

    public function createReto(Request $request)
    {
        $user = $request->user();
        $data = $request->input('data');

        $reto = $this->retoService->editReto($user, $data);

        return response()->json([
            'reto' => $reto,
        ], 200);
    }

    public function editReto(Request $request, $reto)
    {
        $user = $request->user();
        if (!$user->esAdmin()) {
          return response()->json(
              ['error' => 'solo los administradores pueden editar']
              , 403);
        }
        $data = $request->input('data');

        $reto = $this->retoService->editReto($user, $data, $reto);

        return response()->json([
            'reto' => $reto,
        ], 200);
    }
    public function cambiarEstadoReto(Request $request, $reto)
    {
      $user = $request->user();
      if (!$user->esSunnomad()) {
        return response()->json(
            ['error' => 'solo los miembros de sunnomads pueden cambiar estados']
            , 403);
      }

      $reto = Reto::where('id', $reto)->first();
      if (!$reto) {
        return response()->json(
            ['error' => 'reto not found']
            , 404);
      }
      $estado = $request->input('estado');
      $reto = $this->retoService->cambiarEstado($estado, $reto);

      return response()->json([
          'reto' => $reto,
      ], 200);
    }

    public function getRetos(Request $request)
    {
      $user = $request->user();
      $estados = Estado::whereIn('slug', ['pendiente','rechazado'])->get()->pluck(['id']);
      $retos = Reto::whereNotIn('estado_id', $estados)->with('estado')->get();
      foreach ($retos as $reto) {
          $voto = Voto::where('reto_id', $reto->id)
                    ->where('user_id', $user->id)
                    ->first();
          if($voto) {
            $reto->votado = $voto->estado->slug;
          } else {
            $reto->votado = false;
          }
      }
      $siguiente = false;
      $position = Position::where('id',1)->first()->country_id;
      while(!$siguiente && $position <= 15) {
            $siguiente = Reto::whereNotIn('estado_id', $estados)
                  ->where('country_id',$position)
                  ->orderBy('created_at')
                  ->with('estado')
                  ->first();
            if($siguiente) {
                  $voto = Voto::where('reto_id', $siguiente->id)
                            ->where('user_id', $user->id)
                            ->first();
                  if($voto) {
                    $siguiente->votado = $voto->estado->slug;
                  } else {
                    $siguiente->votado = false;
                  }
              }
            $position+=1;
      }

      return response()->json([
          'retos' => $retos,
          'siguiente' => $siguiente
      ], 200);
    }

    public function getAllRetos(Request $request)
    {
      $user = $request->user();
      if (!$user->esSunnomad()) {
        return response()->json(
            ['error' => 'solo los miembros de sunnomads pueden ver todos los retos']
            , 403);
      }
      $retos = Reto::with('estado')->get();

      return response()->json([
          'retos' => $retos,
      ], 200);
    }

    public function saveOutPosition(Request $request, $pais)
    {
      $user = $request->user();
      if (!$user->esSunnomad()) {
        return response()->json(
            ['error' => 'solo los miembros de sunnomads pueden editar']
            , 403);
      }
      $pais = Country::where('name',$pais)->first();
      if(!$pais) {
        return response()->json(
            ['error' => 'pais no encontrado']
            , 404);
      }
      $position = Position::where('id',1)->first();
      $position->country_id = $pais->id;
      $position->save();

      return response()->json([
          'retos' => $position,
      ], 200);
    }

    public function getPaises() {
      $paises = Country::orderBy('id')->get();
      return response()->json([
          'paises' => $paises,
      ], 200);
    }

    public function getReto(Request $request, $reto)
    {
      $user = $request->user();
      $reto = Reto::where('id', $reto)->with('estado')->get();

      return response()->json([
          'reto' => $reto,
      ], 200);
    }
}
