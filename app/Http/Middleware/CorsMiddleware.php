<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    protected $allowedMethods = ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'];

    protected $allowedHeaders = ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization', 'X-Api-Key'];

    protected function injectHeaders($request, $response)
    {
        // if the client has an origin, 'Access-Control-Allow-Origin' has to return that same
        // origin, the wildcard is not valid in that case
        $response->headers->set('Access-Control-Allow-Origin', $request->headers->get('origin') ?: '*');

        // full list of accepted methods
        $response->headers->set('Access-Control-Allow-Methods', implode(',', $this->allowedMethods));

        $requestedHeaders = $request->headers->get('access-control-request-headers') ?: '';

        // some clients require the 'Access-Control-Allow-Headers' header to have only
        // the same values as was sent in 'Access-Control-Request-Headers', so we
        // can't just send the whole list of allowed headers.
        $allowedHeaders = array_intersect(
            array_map(function($elem) {return trim($elem);}, explode(',', $requestedHeaders)),
            array_map(function($elem) {return strtolower($elem);}, $this->allowedHeaders)
        );

        $response->headers->set('Access-Control-Allow-Headers', implode(',', $allowedHeaders));

        // are credentials allowed?
        $response->headers->set('Access-Control-Allow-Credentials', 'true');

        return $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = null;

        // if its a preflight request it's only checking for the Access-Control headers
        // before making the actual call. In that case, stop executing the middlewares chain
        // and just answer with a successful response with the headers.
        if ($request->getMethod() === 'OPTIONS') {
            return $this->injectHeaders($request, response('', 200));
        }

        // continue request lifecycle.
        $response = $next($request);

        // if the response is a string, an array, etc. convert it to a Response object before.
        if (!($response instanceof \Symfony\Component\HttpFoundation\Response)) {
            $response = response($response);
        }

        // add the CORS headers to the response.
        return $this->injectHeaders($request, $response);
    }
}