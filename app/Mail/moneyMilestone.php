<?php

namespace App\Mail;

use App\Models\Dinero;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MoneyMilestone extends Mailable
{
    use Queueable, SerializesModels;

    public $dinero;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Dinero $dinero)
    {
      $this->dinero = $dinero;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.moneyMilestone');
    }
}
