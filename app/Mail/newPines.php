<?php

namespace App\Mail;

use App\Models\Pin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewPines extends Mailable
{
    use Queueable, SerializesModels;

    public $pin;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Pin $pin)
    {
        $this->pin = $pin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.pines');
    }
}
