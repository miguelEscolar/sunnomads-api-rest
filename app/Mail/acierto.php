<?php

namespace App\Mail;

use App\Models\Reto;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Acierto extends Mailable
{
    use Queueable, SerializesModels;

    public $reto;
    public $puntos;
    public $posicion;
    /**
      * Create a new message instance.
      *
      * @return void
      */
    public function __construct(Reto $reto, $puntos, $posicion)
    {
        $this->reto = $reto;
        $this->puntos = $puntos;
        $this->posicion = $posicion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.acierto');
    }
}
