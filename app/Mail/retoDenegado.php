<?php

namespace App\Mail;

use App\Models\Reto;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RetoDenegado extends Mailable
{
    use Queueable, SerializesModels;
    public $reto;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Reto $reto)
    {
        $this->reto = $reto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->view('emails.retoDenegado');
    }
}
