<?php

namespace App\Mail;

use App\Models\Reto;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Fallo extends Mailable
{
    use Queueable, SerializesModels;

    public $reto;
    public $estado;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Reto $reto, $estado)
    {
        $this->reto = $reto;
        $this->estado = $estado;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.fallo');
    }
}
