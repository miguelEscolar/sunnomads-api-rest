<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Authenticatable;

/**
 * App\Models\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read \App\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserData[] $data
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OfferComment[] $offerComments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Opportunity[] $opportunities
 * @property-read \App\Models\Origin $origin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Origin[] $origins
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserComment[] $userComments
 * @property-read \App\Models\UserType $userType
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pin[] $pines
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ranking[] $rankings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reto[] $retos
 * @property-read \App\Models\Rol $rol
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Voto[] $votos
 */
class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use softDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function rol()
    {
        return $this->belongsTo('App\Models\Rol');
    }

    public function votos()
    {
        return $this->hasMany('App\Models\Voto');
    }

    public function pines()
    {
        return $this->hasMany('App\Models\Pin');
    }

    public function retos()
    {
        return $this->hasMany('App\Models\Reto','user_id');
    }

    public function retosPilots()
    {
        return $this->hasMany('App\Models\Reto','pilot_id');
    }

    public function rankings()
    {
        return $this->hasMany('App\Models\Ranking');
    }

    public function chats()
    {
        return $this->hasMany('App\Models\Chat');
    }


    public function esAdmin()
    {
        return ($this->rol->slug === 'admin');
    }

    public function esPiloto()
    {
        return ($this->rol->slug === 'pilot');
    }

    public function esJugador()
    {
        return ($this->rol->slug === 'player');
    }

    public function esSunnomad()
    {
        return ($this->rol->slug === 'admin' || $this->rol->slug === 'pilot');
    }
}
