<?php

namespace App\Models;

/**
 * App\Models\Pin
 *
 * @property-read \App\Models\Tipo $tipo
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 */
class Position extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'positions';

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
