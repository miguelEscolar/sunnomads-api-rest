<?php

namespace App\Models;

/**
 * App\Models\Pin
 *
 * @property-read \App\Models\Tipo $tipo
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 */
class Chat extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chats';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
