<?php

namespace App\Models;

/**
 * App\Models\Hash
 *
 * @mixin \Eloquent
 */
class Hash extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hashes';

}
