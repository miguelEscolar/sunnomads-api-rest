<?php

namespace App\Models;

/**
 * App\Models\Pin
 *
 * @property-read \App\Models\Tipo $tipo
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 */
class Pin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pines';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function tipo()
    {
        return $this->belongsTo('App\Models\Tipo');
    }

    public function pinFiles()
    {
        return $this->hasMany('App\Models\PinFile');
    }
}
