<?php

namespace App\Models;

/**
 * App\Models\Tipo
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pin[] $pines
 * @mixin \Eloquent
 */
class Tipo extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tipos';

    public function pines()
    {
        return $this->hasMany('App\Models\Pin');
    }
}
