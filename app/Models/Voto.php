<?php

namespace App\Models;

/**
 * App\Models\Voto
 *
 * @property-read \App\Models\Estado $estado
 * @property-read \App\Models\Reto $reto
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 */
class Voto extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'votos';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function reto()
    {
        return $this->belongsTo('App\Models\Reto');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }
}
