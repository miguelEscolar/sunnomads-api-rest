<?php

namespace App\Models;

/**
 * App\Models\Pin
 *
 * @property-read \App\Models\Tipo $tipo
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 */
class PinFile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pin_files';

    public function Pin()
    {
        return $this->belongsTo('App\Models\Pin');
    }
}