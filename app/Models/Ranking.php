<?php

namespace App\Models;

/**
 * App\Models\Ranking
 *
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 */
class Ranking extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rankings';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
