<?php

namespace App\Models;

/**
 * App\Models\Pin
 *
 * @property-read \App\Models\Tipo $tipo
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 */
class Country extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    public function positions()
    {
        return $this->hasMany('App\Models\Position');
    }

    public function retos()
    {
        return $this->hasMany('App\Models\Reto');
    }
}
