<?php

namespace App\Models;

/**
 * App\Models\Reto
 *
 * @property-read \App\Models\Estado $estado
 * @property-read \App\Models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Voto[] $votos
 * @mixin \Eloquent
 */
class Reto extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'retos';

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function pilot()
    {
        return $this->belongsTo('App\Models\User','pilot_id');
    }


    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }

    public function votos()
    {
        return $this->hasMany('App\Models\Voto');
    }
}
