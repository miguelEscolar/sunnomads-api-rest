<?php

namespace App\Models;

/**
 * App\Models\Estado
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reto[] $retos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Voto[] $votos
 * @mixin \Eloquent
 */
class Estado extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'estados';

    public function retos()
    {
        return $this->hasMany('App\Models\Reto');
    }

    public function votos()
    {
        return $this->hasMany('App\Models\Voto');
    }
}
