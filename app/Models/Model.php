<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * App\Models\Model
 *
 * @mixin \Eloquent
 */
class Model extends EloquentModel
{
    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTime $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format(Carbon::ISO8601);
    }
}