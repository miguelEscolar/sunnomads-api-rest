var home = (function() {
  //variable global (apaño)
  var idUser = 0;
  var idRole = "";
  var pilotos = [];
  var countries = [];
  window.userInfo = {};

  function _bootstrap() {
    getUser();
    getPilotos();
    getCountries();
    getDonations();
    getPerticipantes();
    getRetos();
  }

  function _logOut() {
    localStorage.clear();
    window.location.replace("/login");
  }

  //Get data form server
  function getUser() {
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/user",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        renderUser(res.user);
        window.userInfo = res.user;
        idUser = res.user.id;
        idRole = res.user.rol.slug;

        if (idRole == "admin") {
          $("#ranking").hide();
          $("#nextReto").hide();
          $("#pendingRetos").show();
          $("#RedesFB").show();
          $("#CambiarPasta").show();
          getAllRetos();
        } else if (idRole == "pilot") {
          $("#ranking").hide();
          $("#nextReto").show();
          $("#pendingRetos").hide();
          $("#createPin").show();
          $("#RedesFB").hide();
          $("#CambiarPasta").show();
        } else {
          $("#ranking").show();
          $("#nextReto").show();
          $("#pendingRetos").hide();
          $("#RedesFB").show();
          $("#CambiarPasta").hide();
        }
        $('.connecting-server').hide();
      },
      error: function(msg) {
        window.location.replace("/login");
      }
    });
  }

  //Get data form server
  function getPilotos() {
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/pilots",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        pilotos = res.pilots;
        $.each(pilotos, function(index, value) {
          $("#retoWho").append($('<option>', {
            value: value.id,
            text: value.name
          }));
        });
        $(".btn-crear-reto").show();
        $('.connecting-server').hide();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  //Get data form server
  function getCountries() {
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/countries",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        var countriesArr = res.paises;
        $.each(countriesArr, function(index, value) {
          $("#retoWhere").append($('<option>', {
            value: value.id,
            text: value.name
          }));
          countries[value.id] = value.name;
        });
        $(".btn-crear-reto").show();
        $('.connecting-server').hide();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  //Get data form server
  function getDonations() {
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/recaudacion",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        var obj = new Object();
        obj.percentage = parseInt(parseInt(res.dinero[0]) / 15000 * 100);
        obj.value = res.dinero[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        renderChart(obj);
        $('.connecting-server').hide();
      },
      error: function(msg) {
        $('.donations').hide();
        $('.connecting-server').hide();
      }
    });
  }

  function getPerticipantes(user) {
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/ranking",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        var players = getPlayers(res.top);
        renderParticipantes(players, res.mine);
        $('.connecting-server').hide();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  function getPlayers(users) {
    var players = [];
    $.each(users, function(index, value) {
      if (parseInt(value.user.rol_id) == 3) {
        players.push(value);
      }
    });
    return players;
  }

  function getRetos() {
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/retos/public",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {

        if (res.retos.length == 0 || idRole == "admin") {
          $("#nextReto").hide();
        } else {
          $("#nextReto").show();
          var pilotoNextReto = "";
          for (var i = 0; i < pilotos.length; i++) {
            if (pilotos[i].id == res.siguiente.pilot_id) {
              pilotoNextReto = pilotos[i].name;
            } else if (res.siguiente.pilot_id === null) {
              pilotoNextReto = 'Todos los pilotos';
            }
          }
          $('.NextRetoPerson').html(pilotoNextReto);
          $('.NextRetoTitle').html(res.siguiente.titulo);
          $('.NextRetoExp').html(res.siguiente.descripcion);
          $('.votoPosJS').removeClass("voto-positivo").removeClass("voto-negativo").removeClass("voto-pendiente").addClass(res.siguiente.votado == 'conseguido' ? "voto-positivo" : "voto-pendiente").attr('idReto', res.siguiente.id);
          $('.votoNegJS').removeClass("voto-positivo").removeClass("voto-negativo").removeClass("voto-pendiente").addClass(res.siguiente.votado == 'fallado' ? "voto-negativo" : "voto-pendiente").attr('idReto', res.siguiente.id);
        }
        renderRetos(res.retos);
        $('.connecting-server').hide();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  function getAllRetos() {
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/retos",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        var AllRetos = [];
        $.each(res.retos, function(index, value) {
          if (value.estado.slug == "pendiente") {
            AllRetos.push(value);
          }
        });
        renderAllRetos(AllRetos);
        $('.connecting-server').hide();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  //Render data in views
  function renderUser(user) {
    $('#avatar-mail').html(user.email);
    $('.position-rank').html(user.posicion + "º");
    $('.position-points').html(user.puntos);

    if (parseInt(user.sinVotar) > 0) {
      $('.sinVotar').html("Tienes"  + user.sinVotar + " reto" + (parseInt(user.sinVotar) == 1 ? "" : "s") + " aún sin votar. no lo dejes así!").show();
    } else {
      $('.sinVotar').hide();
    }
    if (user.posicion == 1) {
      var medal = "Gold-Position";
    } else if (user.posicion == 2) {
      var medal = "Silver-Position";
    } else if (user.posicion == 3) {
      var medal = "Bronze-Position";
    }
    $('.medal').addClass(medal);
  }

  function renderChart(data) {
    $('.chart').attr("data-percent", data.percentage).easyPieChart({
      scaleColor: "#fff",
      lineWidth: 15,
      barColor: '#1abc9c',
      trackColor: "#ecf0f1",
      size: 120,
      animate: 500
    });
    $('.donation-percentage').html(data.percentage + "%");
    $('.donation-value').html(data.value + "€");
  }

  function renderParticipantes(table, mine) {
    if (mine) {
      mine.user = window.userInfo;

      var isInTop = table.find(function(item) {
        return item.user.id === mine.user.id;
      });

      if (!isInTop) {
        table.push(mine);
      }
    }

    table.map(function(item, index) {
      item.pos = index;
      return item;
    });

    $('#tableParticipantes').bootstrapTable('destroy');

    $('#tableParticipantes').bootstrapTable({
      columns: [{
        field: 'id',
        title: 'ID reto',
        visible: false
      }, {
        field: 'user.id',
        title: 'ID usuario',
        visible: false
      }, {
        field: 'pos',
        title: 'Posición',
        class: 'text-center',
        formatter: function(value, row, index, field) {
          return parseInt(value) + 1;
        }
      }, {
        field: 'user.name',
        title: 'Nombre del participante',
        formatter: function(value, row, index, field) {
          if (parseInt(row.pos) >= 0 && parseInt(row.pos) <= 2) {
            return '<i class="medal fa fa-medal"></i> ' + value;
          } else {
            return value;
          }
        }
      }, {
        field: 'retos_votados',
        title: 'Votos',
        class: 'text-center'
      }, {
        field: 'puntos',
        title: 'Puntos',
        class: 'text-center'
      }],
      data: table,
      classes: 'table-no-bordered',
      striped: true,
      sortName: 'pos',
      pagination: true,
      searchOnEnterKey: true,
      pageSize: 25,
      search: true,
      iconsPrefix: 'fa',
      formatNoMatches: function() {
        return 'Aún no hay participantes inscritos!!';
      },
      rowStyle: function(row, index) {
        if (row.pos == 0) {
          return {
            classes: 'Row-Gold-Participantes font-weight-bold'
          };
        }
        if (row.pos == 1) {
          return {
            classes: 'Row-Silver-Participantes font-weight-bold'
          };
        }
        if (row.pos == 2) {
          return {
            classes: 'Row-Bronze-Participantes font-weight-bold'
          };
        }
        if (row.idUser == idUser) {
          return {
            classes: 'Active-Row-Participantes font-weight-bold'
          };
        }
        return {};
      },
      onPostBody: function(e) {
        $(".bootstrap-table").find(".form-control").addClass("form-control-sm");        
      }
    });
  }

  function renderRetos(table) {

    $('#tableRetos').bootstrapTable('destroy');

    $('#tableRetos').bootstrapTable({
      columns: [{
        field: 'id',
        title: 'ID reto',
        visible: false
      }, {
        field: 'titulo',
        title: 'Nombre del reto',
        formatter: function(value, row, index, field) {
          return "<a class='openModalReto' data-idReto=" + row.id + ">" + value + " <small class='text-primary'>(ver)</small></a>"
        }
      }, {
        field: 'pilot_id',
        title: 'Dirigido a',
        class: 'text-center',
        formatter: function(value, row, index, field) {
          for (var i = 0; i < pilotos.length; i++) {
            if (pilotos[i].id == value) {
              return pilotos[i].name;
            } else if (value === null) {
              return 'Todos los pilotos';
            }
          }
        }
      }, {
        field: 'country_id',
        title: 'País',
        class: 'text-center',
        formatter: function(value, row, index, field) {
          return countries[value];
        }
      }, {
        field: 'votado',
        title: 'Tu voto',
        class: 'text-center',
        formatter: function(value, row, index, field) {
          if (window.userInfo.rol_id === 1) {
            if (row.estado_id == 4) {
              return '<a class="btn-decision btn btn-outline-link btn-sm voto-positivo" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-decision btn btn-outline-link btn-sm voto-pendiente" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
            } else if (row.estado_id == 5) {
              return '<a class="btn-decision btn btn-outline-link btn-sm voto-pendiente" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-decision btn btn-outline-link btn-sm voto-negativo" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
            } else {
              return '<a class="btn-decision btn btn-outline-link btn-sm voto-pendiente" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-decision btn btn-outline-link btn-sm voto-pendiente" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
            }
          } else {
            if (value == 'conseguido') {
              return '<a class="btn-voto btn btn-outline-link btn-sm voto-positivo" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-voto btn btn-outline-link btn-sm voto-pendiente" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
            } else if (value == 'fallado') {
              return '<a class="btn-voto btn btn-outline-link btn-sm voto-pendiente" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-voto btn btn-outline-link btn-sm voto-negativo" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
            } else {
              return '<a class="btn-voto btn btn-outline-link btn-sm voto-pendiente" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-voto btn btn-outline-link btn-sm voto-pendiente" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
            }
          }
        }
      }],
      data: table,
      classes: 'table-no-bordered',
      striped: true,
      sortName: 'fecha',
      pagination: true,
      searchOnEnterKey: true,
      pageSize: 25,
      search: true,
      iconsPrefix: 'fa',
      formatNoMatches: function() {
        return 'Aún no hay retos guardados!!';
      },
      rowStyle: function(row, index) {
        if (row.user_id == idUser) {
          return {
            classes: 'Active-Row-Participantes font-weight-bold'
          };
        }
        return {};
      },
      onPostBody: function(e) {
        $(".bootstrap-table").find(".form-control").addClass("form-control-sm");
      }
    });
  }

  function openModalReto(idReto) {
    console.log("Dentro");
    $("#createReto").hide();
    $('.connecting-server').show();
    $.ajax({
      type: "GET",
      url: "api/reto/get/" + idReto,
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        $('#modalNuevoReto').modal('show');
        $("#retoDesc").val(res.reto[0].descripcion);
        $("#retoTitle").val(res.reto[0].titulo);
        $("#retoWhere").val(res.reto[0].country_id);
        $("#retoWho").val(res.reto[0].pilot_id == null ? 0 : res.reto[0].pilot_id);
        $('.connecting-server').hide();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  function renderAllRetos(table) {

    $('#tablePendingRetos').bootstrapTable('destroy');

    $('#tablePendingRetos').bootstrapTable({
      columns: [{
        field: 'id',
        title: 'ID reto',
        visible: false
      }, {
        field: 'titulo',
        title: 'Nombre del reto',
        formatter: function(value, row, index, field) {
          return "<a class='openModalReto' data-idReto=" + row.id + ">" + value + " <small class='text-primary'>(ver)</small></a>"
        }
      }, {
        field: 'pilot_id',
        title: 'Dirigido a',
        class: 'text-center',
        visible: false
      }, {
        field: 'estado.slug',
        title: '¿Lo aceptas?',
        class: 'text-center',
        formatter: function(value, row, index, field) {

          if (value == 'aceptado') {
            return '<a class="btn-pending-retos btn btn-outline-link btn-sm voto-positivo" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-pending-retos btn btn-outline-link btn-sm voto-pendiente" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
          } else if (value == 'rechazado') {
            return '<a class="btn-pending-retos btn btn-outline-link btn-sm voto-pendiente" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-pending-retos btn btn-outline-link btn-sm voto-negativo" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
          } else {
            return '<a class="btn-pending-retos btn btn-outline-link btn-sm voto-pendiente" data-voto="1"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-up"></a></i><a class="btn-pending-retos btn btn-outline-link btn-sm voto-pendiente" data-voto="0"' + 'data-idReto=' + row.id + '><i class="fa fa-fw fa-thumbs-down"></a></i>';
          }
        }
      }],
      data: table,
      classes: 'table-no-bordered',
      striped: true,
      sortName: 'estado.slug',
      pagination: true,
      searchOnEnterKey: true,
      pageSize: 10,
      search: false,
      iconsPrefix: 'fa',
      formatNoMatches: function() {
        return 'No quedan retos por validar!!';
      },
      onPostBody: function(e) {
        $(".bootstrap-table").find(".form-control").addClass("form-control-sm");
      }
    });
  }

  $(document).on('click', '.btn-voto', function(event) {
    if ($(event.target).attr("idReto") > 0) {
      var idReto = $(event.target).attr("idReto");
      var voto = $(event.target).attr("data-voto");
      updateVotoReto(idReto, voto);
    } else {
      var idReto = $(event.target).closest('a').attr("data-idReto");
      var voto = $(event.target).closest('a').attr("data-voto");
      updateVotoReto(idReto, voto);
    }
  });

  $(document).on('click', '.btn-decision', function(event) {
    if ($(event.target).attr("idReto") > 0) {
      var idReto = $(event.target).attr("idReto");
      var voto = $(event.target).attr("data-voto");
      updateDesicionReto(idReto, voto);
    } else {
      var idReto = $(event.target).closest('a').attr("data-idReto");
      var voto = $(event.target).closest('a').attr("data-voto");
      updateDesicionReto(idReto, voto);
    }
  });

  $(document).on('click', '.btn-pending-retos', function(event) {
    console.log("clicked");
    if ($(event.target).attr("idReto") > 0) {
      var idReto = $(event.target).attr("idReto");
      var voto = $(event.target).attr("data-voto");
      console.log(idReto + "-" + voto);
      updatePendingReto(idReto, voto);
    } else {
      console.log(idReto + "-" + voto);
      var idReto = $(event.target).closest('a').attr("data-idReto");
      var voto = $(event.target).closest('a').attr("data-voto");
      updatePendingReto(idReto, voto);
    }
  });

  $(document).on('click', '.openModalReto', function(event) {
    var idReto = $(event.target).closest("a").attr("data-idReto");
    console.log(idReto);
    openModalReto(idReto);
  });

  $(document).on('click', '.btn-crear-reto', function(event) {
    $("#createReto").show();
    $('#modalNuevoReto').modal('show');
  });    
  

  function updateVotoReto(idReto, voto) {
    $('.connecting-server').show();
    $.ajax({
      type: "POST",
      url: "api/user/vote/" + idReto,
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      data: JSON.stringify({
        "vote": parseInt(voto) == 1 ? "conseguido" : "fallado"
      }),
      contentType: "application/json",
      success: function(res) {
        $('.connecting-server').hide();
        getUser();
        getRetos();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  function updateDesicionReto(idReto, voto) {
    $('.connecting-server').show();
    $.ajax({
      type: "POST",
      url: "api/reto/estado/" + idReto,
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      data: JSON.stringify({
        "estado": parseInt(voto) == 1 ? "conseguido" : "fallado"
      }),
      contentType: "application/json",
      success: function(res) {
        $('.connecting-server').hide();
        getUser();
        getRetos();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  function updatePendingReto(idReto, voto) {
    $('.connecting-server').show();
    $.ajax({
      type: "POST",
      url: "api/reto/estado/" + idReto,
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      data: JSON.stringify({
        "estado": parseInt(voto) == 1 ? "aceptado" : "rechazado"
      }),
      contentType: "application/json",
      success: function(res) {
        $('.connecting-server').hide();
        getUser();
        getRetos();
        getAllRetos();
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  }

  $('#createReto').on('click', function() {
    $('#modalNuevoReto').modal('hide');
    $('.connecting-server').show();
    $.ajax({
      type: "POST",
      url: "api/reto/create",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
        "Content-Type": "application/json"
      },
      data: JSON.stringify({
        data: {
          "descripcion": $("#retoDesc").val(),
          "titulo": $("#retoTitle").val(),
          "country_id": $("#retoWhere").val(),
          "pilot_id": parseInt($("#retoWho").val()) == 0 ? "" : $("#retoWho").val(),
          "video": "youtube"
        }
      }),
      success: function(res) {
        $('.connecting-server').hide();
        getAllRetos();
        $('#modalNuevoRetoOK').modal('show');
      },
      error: function(msg) {
        $('.connecting-server').hide();
        $('.error-alert > span').html("El servidor ha respondido: " + msg.responseJSON.error);
        $('.error-alert').show();
        setTimeout(function() {
          $('.error-alert').hide();
        }, 3000);
      }
    });
  });

  $(document).ready(function() {
    $('.closeParticipants').click(function(e) {
      e.preventDefault();
      Cookies.set('closeParticipants', 'closed');
    });
    if (Cookies.get('closeParticipants') === 'closed') {
      $('.alertParticipants').hide();
    }
    $('.closeRetos').click(function(e) {
      e.preventDefault();
      Cookies.set('closeRetos', 'closed');
    });
    if (Cookies.get('closeRetos') === 'closed') {
      $('.alertRetos').hide();
    }
    $('#modalNuevoReto').on('show.bs.modal', function (e) {
        $("#retoDesc").val("");
        $("#retoTitle").val("");
        $("#retoWhere").val(1);
        $("#retoWho").val(0);
    });    
  });


  //Public functions
  return {
    bootstrap: _bootstrap,
    logOut: _logOut
  }
})();

home.bootstrap();
this.logOut = home.logOut;
