var googleMapsController = (function() {

  //GoogleMaps
  var my_map;
  var centerMap;
  var markers = [];
  var directionsService;
  var directionsDisplay;
  var infowindow;

  var $htmlOrBody = $('html, body'), // scrollTop works on <body> for some browsers, <html> for others
      scrollTopPadding = 8;

  function _getPinsAndChallenges() {
    $.ajax({
      type: "GET",
      url: "api/pines",
      dataType: "json",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      success: function(res) {
        initGoogleMaps(res.pines);
      },
      error: function(msg) {
        window.location.replace("/login");
      }
    });
  }

  function initGoogleMaps(pines) {

    centerMap = {
      lat: 50.107389,
      lng: 32.422715
    };
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;

    my_map = new google.maps.Map(document.getElementById('mapChallenges'), {
      scaleControl: true,
      zoom: 4,
      center: centerMap,
      disableDefaultUI: false,
      draggableCursor: "crosshair",
      keyboardShortcuts: false,
      panControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
      },
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
        position: google.maps.ControlPosition.LEFT_BOTTOM
      },
      styles: [{
        featureType: "poi",
        stylers: [{
          visibility: "off"
        }]
      }]
    });

    google.maps.event.addListener(my_map, "click", function(event) {
        hideAllInfoWindows(my_map);
    });

    var rendererOptions = {
      suppressMarkers: true
    };

    size_map_to_window();
    directionsDisplay.setMap(my_map);
    directionsDisplay.setOptions(rendererOptions);
    calcRoute(pines);
    window.onresize = size_map_to_window;

  }

  function size_map_to_window() {
    $("#mapChallenges").height('100vh');
  }

  function hideAllInfoWindows(my_map) {
    markers.forEach(function(marker) {
        marker.infowindow.close(my_map, marker);
    }); 
  }

  function calcRoute(pines) {

/*
    var pines = [
      {
        titulo: "Título 1 para pruebas",
        descripcion: "Descripción 1",
        GPS: "40.428269, -3.698810",
        archivo: [
          "https://www.youtube.com/watch?v=JD9-9R7hai4",
          "https://www.dropbox.com/s/wuxdzzynvo11rcc/IMG_20180727_115755_734.jpg?raw=1",
          "https://www.dropbox.com/s/j69y6a2c6b3fbud/IMG_20180727_095325_160.jpg?raw=1"
        ],
        texto_lugar: "Sant Cugat",
        tipo: 
        {
          name: "video"
        },
        updated_at: "23/08/2018"
      }
    ];
*/
    var waypts = pines.map(function(pin) {
      var res = {}
      var cords = pin.GPS.split(', ');
      res.location = new google.maps.LatLng(cords[0], cords[1]);
      res.stopover = true;
      return res;
    });

    var start = new google.maps.LatLng(40.428269, -3.698810);
    var end = new google.maps.LatLng(47.841037, 106.769864);
    var icon_start = './img/pin_start.png'
    var icon_end = './img/pin_finish.png'

    var iconos = [
      './img/pin_camera.png',
      './img/pin_video.png',
      './img/pin_text.png',
      './img/pin_broken.png',
      './img/pin_challenge.png',
      './img/pin_pitstop.png',
      ''
    ];

    var request = {
      origin: start,
      destination: end,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    };

    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        var maxNumber = response.routes[0].legs.length;
        createMarker(response.routes[0].legs[0].start_location, icon_start);
        createMarker(response.routes[0].legs[response.routes[0].legs.length - 1].end_location, icon_end);
        pines.forEach(function(valor, indice, array) {
          
          var cords = valor.GPS.split(', ');

          var contentString = '<div class="InfoWcontent">';
          contentString = contentString + '<h6>Tipo de pin: ' + valor.tipo.name  + '</h6>';
          if (valor.archivos.length > 0) {
            contentString = contentString + '<p><small>Número de archivos ' + valor.archivos.length + '</small></p>';
          }
          contentString = contentString + '<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">';
            contentString = contentString + '<ol class="carousel-indicators">';
            $.each(valor.archivos, function(index, value) {
              contentString = contentString + '<li data-target="#carouselExampleIndicators" data-slide-to="' + index + '" ' + (index == 0 ? 'class="active"' : '') + '></li>';
            });
            contentString = contentString + '</ol>';
            contentString = contentString + '<div class="carousel-inner">';
            $.each(valor.archivos, function(index, value) {
              contentString = contentString + '<div class="carousel-item ' + (index == 0 ? 'active' : '') + '">';
                  if (value !== null && value.search("youtube") != -1) {
                      contentString = contentString + '<div class="containerYoutube"><iframe src="' + value.replace("watch?v=", "embed/") + '" frameborder="0" allowfullscreen class="videoYoutube"></iframe></div>'; 
                  }else if (value !== null && value.search(".") != -1) {
                      contentString = contentString + '<div class="InfoWimage" style="background-image: url(' + value + ');"></div>';
                  }
              contentString = contentString + '</div>';
            });    
            contentString = contentString + '</div>';
            contentString = contentString + '<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">';
              contentString = contentString + '<span class="carousel-control-prev-icon" aria-hidden="true"></span>';
              contentString = contentString + '<span class="sr-only">Previous</span>';
            contentString = contentString + '</a>';
            contentString = contentString + '<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">';
              contentString = contentString + '<span class="carousel-control-next-icon" aria-hidden="true"></span>';
              contentString = contentString + '<span class="sr-only">Next</span>';
            contentString = contentString + '</a>';
          contentString = contentString + '</div>';

          contentString = contentString + '<h5 class="mt-2">'+ valor.titulo +'</h5>' +
              '<div id="bodyContent">' +
                '<p><small>Creado el ' + Date.parse(valor.updated_at).toString('M/d/yyyy') + '</small></p>' +
                '<p>'+ valor.texto_lugar +'</p>' +
                '<p>'+ valor.descripcion +'</p>' +
              '</div>' +
            '</div>';   

          if(valor.tipo.name != "punto") {
            createMarker(new google.maps.LatLng(cords[0], cords[1]), iconos[parseInt(valor.tipo.id) - 1], contentString);
          }

        });
        setTimeout(function() {
          infowindow.open(my_map, markers[markers.length - 1]);
          //panToMarkerInfoWindow();
        }, 2000);                        
      }
    });

  }

  function createMarker(position, icon, contentString) {

    infowindow = new google.maps.InfoWindow({
      content: contentString,
      disableAutoPan: false
    });

    var marker = new google.maps.Marker({
      position: position,
      map: my_map,
      icon: {
        url: icon, 
        scaledSize: new google.maps.Size(33, 48)
      },
      infowindow: infowindow,
      animation: google.maps.Animation.DROP
    });

    google.maps.event.addListener(marker, 'click', function() {
      hideAllInfoWindows(my_map);
      this.infowindow.open(my_map, this);
      //$(".chatContainerTexts").hide();
      $("#chatModal").modal('hide');
      //panToMarkerInfoWindow();
    });

    markers.push(marker);

  }

  function panToMarkerInfoWindow () {
      var mapBounds = my_map.getBounds();
      var markerPosition = markers[markers.length - 1].position;
      var markerPositionPaned = new google.maps.LatLng(mapBounds.getNorthEast().lat(), markerPosition.lng());
      slowPanTo(my_map, markerPositionPaned, 50, 1500);
  }

  $(document).ready(function() {
      _getPinsAndChallenges();

      $('.chatContainer').click(function(e) {
          hideAllInfoWindows(my_map);
          $("#chatModal").modal('toggle');  
          $(".chatContainerTexts").scrollTop($(".chatContainerTexts")[0].scrollHeight);
      });  
      $('#enviaChat').click(function(e) {
          saveChatServer($("#msgToSend").val());
          $("#msgToSend").val("");
      }); 
      $('#msgToSend').on('focus', function() {
          //$('html, body').scrollTop($(document).height());
      });       
      initChat();
              
  });

  function initChat(){
      getChatServer();
      setTimeout(initChat, 5000);
  }


  function saveChatServer(text) {
    $.ajax({
      type: "POST",
      url: "api/chat/write",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      data: JSON.stringify({
        "msg": text
      }),
      contentType: "application/json",
      success: function(res) {
        getChatServer();
      },
      error: function(msg) {
      }
    });
  }

  function getChatServer() {
    $.ajax({
      type: "GET",
      url: "api/chat/get",
      headers: {
        "Authorization": "Bearer" + localStorage.getItem('token'),
      },
      contentType: "application/json",
      success: function(res) {
        showChatMsg(res.chat);
      },
      error: function(msg) {
      }
    });    
  }

  function showChatMsg(msgArr) {
    var fechaBrowser = Cookies.get('FechaUltimoMensaje');
    $(".chatContainerTexts > ul > li").each(function(){
        $(this).remove();
    });
    if (msgArr.length == 0) {
      $(".chatContainerTexts > ul").append('<li>' +
          '<span class="chatMsg">Se el primero en escribirnos un mensaje</span>' +                             
      '</li>');
    } else{
      var ultimaFecha = "";
      var msgSinLeer = 0;
      $.each(msgArr, function( index, msg ){
          $(".chatContainerTexts > ul").append('<li class="' + (index > 0 ? "chatLine": "") + '">' +
              '<div>' +
                  '<span class="chatTit">' + msg.user_name + '</span>' +
              '</div>' +
              '<span class="chatMsg">' + 
                '<span class="chatTime mr-2"><i>' + Date.parse(msg.created_at).toString('HH:mm') + '</i></span>' +
                msg.msg + 
              '</span>' +                             
          '</li>');
          ultimaFecha = msg.created_at;
          if(Date.parse(msg.created_at).isAfter(Date.parse(fechaBrowser)) && !(($("#chatModal").data('bs.modal') || {})._isShown)) {
            msgSinLeer = msgSinLeer + 1;
          }
      });
      if (msgSinLeer == 0) {
          Cookies.set('FechaUltimoMensaje', ultimaFecha);
          $(".msgSinLeer").hide().html("");
      } else if (($("#chatModal").data('bs.modal') || {})._isShown){
          $(".msgSinLeer").hide().html("");  
      }else{
          $(".msgSinLeer").show().html(msgSinLeer);
      }
    }
  }

  


  var slowPanTo = function(map, endPosition, n_intervals, T_msec) {
    var f_timeout, getStep, i, j, lat_array, lat_delta, lat_step, lng_array, lng_delta, lng_step, pan, ref, startPosition;
    getStep = function(delta) {
      return parseFloat(delta) / n_intervals;
    };
    startPosition = map.getCenter();
    lat_delta = endPosition.lat() - startPosition.lat();
    lng_delta = endPosition.lng() - startPosition.lng();
    lat_step = getStep(lat_delta);
    lng_step = getStep(lng_delta);
    lat_array = [];
    lng_array = [];
    for (i = j = 1, ref = n_intervals; j <= ref; i = j += +1) {
      lat_array.push(map.getCenter().lat() + i * lat_step);
      lng_array.push(map.getCenter().lng() + i * lng_step);
    }
    f_timeout = function(i, i_min, i_max) {
      return parseFloat(T_msec) / n_intervals;
    };
    pan = function(i) {
      if (i < lat_array.length) {
        return setTimeout(function() {
          map.panTo(new google.maps.LatLng({
            lat: lat_array[i],
            lng: lng_array[i]
          }));
          return pan(i + 1);
        }, f_timeout(i, 0, lat_array.length - 1));
      }
    };
    return pan(0);
  };

  return {
    getPinsAndChallenges: _getPinsAndChallenges
  }

})();

//this.getPinsAndChallenges = googleMapsController.getPinsAndChallenges;
