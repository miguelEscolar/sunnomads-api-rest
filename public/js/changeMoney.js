debugger
$.ajax({
  type: "GET",
  url: "api/recaudacion",
  dataType: "json",
  headers: {
    "Authorization": "Bearer" + localStorage.getItem('token'),
  },
  success: function(res) {
    $("#inputRecaudación").val(res.dinero[0]);
  },
  error: function(msg, error) {
    window.alert(error);
  }
});

function modificarRecoudacion(form) {

  var sendInfo = {
    dinero: form.elements["inputRecaudación"].value,
  }

  $.ajax({
    type: "POST",
    url: "api/recaudacion",
    dataType: "json",
    contentType: "application/json",
    headers: {
      "Authorization": "Bearer" + localStorage.getItem('token'),
    },
    success: function(msg) {
      window.location.replace("/");
    },
    error: function(msg, error) {
      window.alert(error);
    },
    data: JSON.stringify(sendInfo)
  });

  return false;
}
