function register(form) {

  var sendInfo = {
    nameUser: form.elements["inputName"].value,
    email: form.elements["inputEmail"].value,
    password: form.elements["inputPassword"].value,
    repeatPassword: form.elements["inputrRepeatPassword"].value
  }
  if (form.elements["inputPassword"].value === form.elements["inputrRepeatPassword"].value) {
    $.ajax({
      type: "POST",
      url: "api/register",
      dataType: "json",
      contentType: "application/json",
      success: function(msg) {
        localStorage.setItem('token', msg.token);
        window.location.replace("/");
      },
      error: function(msg, error) {
        if (msg.responseText === '{"email":["The email has already been taken."]}') {
          window.alert('Ese email ya esta en uso');
        } else if (msg.responseText === '{"password":["The password must be at least 6 characters."]}'){
          window.alert('La contraseña debe de tener mas de 6 caracteres');
        } else {
          window.alert('Ha ocurrido un error con el servidor, disculpe las molestias');
        }
      },
      data: JSON.stringify(sendInfo)
    });
  } else {
    window.alert('Las dos contraseñas proporcionadas no coinciden');
  }

  return false;
}
