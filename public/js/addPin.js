
$(document).ready(function() {

  navigator.geolocation.getCurrentPosition(function(location) {
    $("#inputGps").val(location.coords.latitude + ", " + location.coords.longitude);
  }, function(error) {
    $("#inputGps").attr("placeholder", "Introduce coordenadas GPS manuales");
  },{ enableHighAccuracy:true });


  $('#AddNewFile').click(function() {
      var insertAfter = $("#GroupArchivoUrl");
      insertAfter.after('<div class="form-group"><label>Añade otro archivo a este pin</label><input type="text" class="form-control form-control-sm other-file" placeholder="Añade otro archivo" required></div>');
  });


});

function createPin(form) {

  var sendInfo = {
    data:{
      titulo: form.elements["inputTitulo"].value,
      descripcion: form.elements["inputDescription"].value,
      GPS: form.elements["inputGps"].value,
      archivo: form.elements["inputArchivoUrl"].value,
      texto_lugar: form.elements["inputTextoLugar"].value,
      tipo_id: form.elements["inputType"].value,
    }
  }

  $.ajax({
    type: "POST",
    url: "api/Pin/create",
    dataType: "json",
    headers: {
      "Authorization": "Bearer" + localStorage.getItem('token'),
    },
    contentType: "application/json",
    success: function(res) {
      if ($('.other-file').length > 0) {
        aditionaFilesIntoPin (res.pin.id);   
      } else {
        window.location.replace("/");      
      }
    },
    error: function(msg, error) {
      window.alert('Error ' + msg.status + ' ' + msg.responseJSON.error);
    },
    data: JSON.stringify(sendInfo)
  });
  
  return false;
}

function aditionaFilesIntoPin (idPin) {
  var extraFiles = [];
  $('.other-file').each(function (index, value) { 
      extraFiles.push($(value).val());
  }); 

  $.ajax({
    type: "POST",
    url: "api/Pin/addFiles/" + idPin,
    dataType: "json",
    headers: {
      "Authorization": "Bearer" + localStorage.getItem('token'),
    },
    contentType: "application/json",
    success: function(res) {
      window.location.replace("/");
    },
    error: function(msg, error) {
      window.alert('Error ' + msg.status + ' ' + msg.responseJSON.error);
    },
    data: JSON.stringify(extraFiles)
  });

}




