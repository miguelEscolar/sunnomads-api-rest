function login(form) {

  var sendInfo = {
    email: form.elements["inputEmail"].value,
    password: form.elements["inputPassword"].value
  }

  $.ajax({
    type: "POST",
    url: "api/login",
    dataType: "json",
    success: function(msg) {
      localStorage.setItem('token', msg.token);
      window.location.replace("/");
    },
    error: function(msg, error) {
      window.alert('Error ' + msg.status + ' ' + msg.responseJSON.error);
    },
    data: sendInfo
  });
  return false;
}
